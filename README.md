# Kyrmir-TDM

## License
The code is licensed under GNU GENERAL PUBLIC LICENSE, with the exception of the camera code, which was written by Patrix. 
You can find it here: https://gothic-online.com.pl/script/35

## Project status
The project is still being slowly developed, and in the future, I plan to refactor and update it.

In its current version, there are many incorrect solutions, but if you want to contribute to the project, feel free to update it.