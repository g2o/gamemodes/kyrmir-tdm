
setTimer(Round.Manager.checkRound.bindenv(Round.Manager), 100, 0);

addEventHandler("onInit", Round.Manager.init.bindenv(Round.Manager));

addEventHandler("onPlayerJoin", Round.Manager.playerJoin.bindenv(Round.Manager));

addEventHandler("Guild.onPointsUpdate", function(guildId, points)
{
    getRoundManager().pointsUpdate(points);
});

addEventHandler("Round.onRoundStart", Round.Manager.roundStart.bindenv(Round.Manager));
