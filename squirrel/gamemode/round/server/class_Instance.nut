
class Round.Instance
{
    _beginingTime = null;
    _targetPoints = 0;
    
    _experienceBonus = 0;
    _started = false;

    constructor(beginingTime, roundPoints)
    {
        _beginingTime = beginingTime;
        _targetPoints = roundPoints;
        
        _experienceBonus = 0;
        _started = false;
    }

//:public
    function start()
    {
        _started = true;
        
        Packet(PacketIDs.Round, Round_PacketIDs.Start).sendToAll(RELIABLE_ORDERED);   
            callEvent("Round.onRoundStart");
    }

    function stop()
    {
        _started = false;
        
        Packet(PacketIDs.Round, Round_PacketIDs.Stop).sendToAll(RELIABLE_ORDERED);   
            callEvent("Round.onRoundStop");
    }

    function isStarted() { return _started; }
    
    function getBeginingTime() { return _beginingTime; }

    function setTargetPoints(targetPoints)
    {
        if(targetPoints != _targetPoints)
        {
            local packet = Packet(PacketIDs.Round, Round_PacketIDs.Target);
                packet.writeInt16(_targetPoints = targetPoints);
            packet.sendToAll(RELIABLE);   
        }
    }

    function getTargetPoints() { return _targetPoints; }

    function setExperienceBonus(bonus)
    {
        if(bonus != _experienceBonus)
        {
            local packet = Packet(PacketIDs.Round, Round_PacketIDs.Bonus);
                packet.writeFloat(_experienceBonus = bonus);
            packet.sendToAll(RELIABLE);   
        }
    }

    function getBonus() { return _experienceBonus; }
}