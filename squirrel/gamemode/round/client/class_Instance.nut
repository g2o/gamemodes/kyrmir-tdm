
class Round.Instance
{
    _beginingTime = null;
    _targetPoints = 0;

    _experienceBonus = 0;
    _started = false;

    constructor(beginingTime, roundPoints, bonus)
    {
        _beginingTime = beginingTime;
        _targetPoints = roundPoints;

        _experienceBonus = bonus;
        _started = false;
    }

//:public
    function start()
    {
            _started = true;
        callEvent("Round.onRoundStart");
    }

    function stop()
    {
            _started = false;
        callEvent("Round.onRoundStop");
    }

    function isStarted() { return _started; }
    
    function getBeginingTime() { return _beginingTime; }

    function setTargetPoints(targetPoints)
    {
        if(targetPoints != _targetPoints)
        {
            callEvent("Round.onRoundTarget", _targetPoints = targetPoints);
        }
    }

    function getTargetPoints() { return _targetPoints; }

    function setExperienceBonus(bonus)
    {
        if(bonus != _experienceBonus)
        {
            callEvent("Round.onRoundBonus", _experienceBonus = bonus);
        }
    }

    function getBonus() { return _experienceBonus; }
}