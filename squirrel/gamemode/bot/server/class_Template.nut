
class Bot.Template
{
    _instance = null;
    _identifier = null;

    _groupId = -1;

    _level = 0;
    _magicLevel = 0;

    _health = 40;

    _damageType = -1;
    _damage = 0;

    _protection = null;
    _weaponSkill = null;
    
    _meleeWeaponId = -1;
    _rangedWeaponId = -1;
    _armorId = -1;
    _helmetId = -1;

    _aiState = false;

    _respawnTime = 0;
    _respawnRequirement = null;

    _warnTime = 5;

    _hitDistance = 250;
    _attackSpeed = 3000;

    _detectionDistance = 1000;
    _chaseDistance = 1200;
    _fightDistance = 120;

    _initiativeFunction = null;
    
    _routineFunction = null;
    _routineFreq = 0;

    constructor(instance, identifier)
    {
        _instance = instance;
        _identifier = identifier;

        _groupId = -1;
        
        _level = 0;
        _magicLevel = 0;

        _health = 40;
        
        _damageType = -1;
        _damage = 0;

        _protection = {};
        _weaponSkill = {};

        _meleeWeaponId = -1;
        _rangedWeaponId = -1;
        _armorId = -1;
        _helmetId = -1;

        _aiState = false;

        _respawnTime = 0;
        _respawnRequirement = null;

        _warnTime = 0;

        _hitDistance = 250;
        _attackSpeed = 3000;

        _detectionDistance = 1000;

        _chaseDistance = 1200;
        _fightDistance = 200;
        
        _initiativeFunction = null;
        _routineFunction = null;
    }

//:public
    function getInstance() { return _instance; }

    function getIdentifier() { return _identifier; }

    function setGroup(groupId)
    {
        _groupId = groupId;
    }

    function getGroup() { return _groupId; }
    
    function setLevel(level)
    {
        _level = level;
    }

    function getLevel() { return _level; }

    function setMagicLevel(magicLevel)
    {
        _magicLevel = magicLevel;
    }

    function getMagicLevel() { return _magicLevel; }

    function setHealth(health)
    {
        _health = health;
    }

    function getHealth() { return _health; }

    function setDamage(damageType, damage) 
    {
        _damageType = damageType;
        _damage = damage;
    }

    function getDamage() { return _damage; }

    function getDamageType() { return _damageType; }

    function setProtection(protectionType, protection)
    {
        _protection[protectionType] <- protection;
    }

    function getProtection(protectionType) { return protectionType in _protection ? _protection[protectionType] : 0; }

    function setProtection(protectionType, protection)
    {
        _protection[protectionType] <- protection;
    }

    function setSkillWeapon(skillType, skill) 
    {
        _weaponSkill[skillType] <- skill;
    }

    function getSkillWeapon(skillType) { return skillType in _weaponSkill ? _weaponSkill[skillType] : 0; }

    function setMeleeWeapon(meleeWeaponId)
    {
        _meleeWeaponId = meleeWeaponId;
    }

    function getMeleeWeapon() { return _meleeWeaponId; }

    function setRangedWeapon(rangedWeaponId)
    {
        _rangedWeaponId = rangedWeaponId;
    }

    function getRangedWeapon() { return _rangedWeaponId; }
    
    function setArmor(armorId)
    {
        _armorId = armorId;
    }

    function getArmor() { return _armorId; }

    function setHelmet(helmetId)
    {
        _helmetId = helmetId;
    }

    function getHelmet() { return _helmetId; }

    function setRespawnTime(respawnTime)
    {
        _respawnTime = respawnTime * 1000;
    }

    function getRespawnTime() { return _respawnTime; }

    function setRespawnRequirement(respawnRequirement)
    {
        _respawnRequirement = respawnRequirement;
    }

    function checkRespawnRequirement()
    {
        if(_respawnRequirement != null)
            return _respawnRequirement();

        return true;
    }

    function setWarnTime(warnTime)
    {
        _warnTime = warnTime * 1000;
    }

    function getWarnTime() { return _warnTime; }
    
    function setDetectionDistance(detectionDistance)
    {
        _detectionDistance = detectionDistance;
    }

    function getDetectionDistance() { return _detectionDistance; }

    function setChaseDistance(chaseDistance)
    {
        _chaseDistance = chaseDistance;
    }

    function getChaseDistance() { return _chaseDistance; }

    function setHitDistance(hitDistance)
    {
        _hitDistance = hitDistance;
    }

    function getHitDistance() { return _hitDistance; }

    function setAttackSpeed(attackSpeed)
    {
        _attackSpeed = attackSpeed;
    }

    function getAttackSpeed() { return _attackSpeed; }

    function setInitiativeFunction(func)
    {
        _initiativeFunction = func;
    }

    function getInitiativeFunction() { return _initiativeFunction; }

    function setRoutineFunction(func, freq = 6)
    {
        _routineFunction = func;
        _routineFreq = freq * 1000;
    }

    function getRoutineFunction() { return _routineFunction; }

    function getRoutineFreq() { return _routineFreq; }
}
