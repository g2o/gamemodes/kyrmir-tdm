
class MySQL.Query
{
    _result = null;
    _query = null;

    _connection = null;

    constructor(connection, query)
    {
        _connection = connection;
            _query = query;        
    }

//:public
    function exec()
    {
        _result = _connection.query(_query);
            return this;
    }
    
    function assoc()
    {
        return _result != null ? mysql_fetch_assoc(_result) : null;
    }

    function row()
    {
        return _result != null ? mysql_fetch_row(_result) : null;
    }

    function row_num()
    {
        return _result != null ? mysql_num_rows(_result) : null;
    }

    function getQuery() { return _query; }

    function getResult() { return _result; }
}