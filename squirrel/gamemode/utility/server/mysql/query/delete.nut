
class MySQL.QueryDelete 
{
    _result = null;
        
    _table = null; 
    _where = null;    
    
    _connection = null;

    constructor(connection, table)
    {
        _connection = connection;
     
        _table = table;
        _where = null;   
    }

//:private 
    function generateQuery()
    {
        return "DELETE FROM " + _table + (_where != null ? (" " + _where) : "") + ";";
    }

//:public
    function where(...)
    {
        local where = "",
            lastIndex = vargv.len() - 1;

        foreach(i, arg in vargv)
        {
            if(i == lastIndex)
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "'"); 
            else 
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "' " + vargv[i][3] + " "); 
        }

        _where = " WHERE " + where;
            return this;
    }

    function exec()
    {
        _result = _connection.query(generateQuery());
            return this;
    }

    function getQuery() { return generateQuery(); }

    function getResult() { return _result; }    
}
