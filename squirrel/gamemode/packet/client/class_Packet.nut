
_Packet <- Packet;

class Packet extends _Packet
{
    constructor(packetId, subId = -1)
    {
        base.constructor();
        base.writeInt8(packetId);

        if(subId != -1)
            base.writeInt8(subId);
    }

    function send(relability = RELIABLE)
    {
        base.send(relability);
    }
}