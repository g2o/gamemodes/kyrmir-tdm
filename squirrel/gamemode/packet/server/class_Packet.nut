
_Packet <- Packet;

class Packet extends _Packet
{
    constructor(packetId, subId = -1)
    {
        base.constructor();
        base.writeInt8(packetId);

        if(subId != -1)
            base.writeInt8(subId);
    }

    function send(playerId, relability = RELIABLE)
    {
        base.send(playerId, relability);
    }

    function sendToArray(arrayList, relability = RELIABLE)
    {
        foreach(playerId in arrayList)
        {
            base.send(playerId, relability);
        }
    }

    function sendToWorld(world, virtualWorld, relability = RELIABLE)
    {
        local maxSlots = getMaxSlots();

        for(local id = 0; id < maxSlots; id++)
        {
            if(isPlayerConnected(id) && world == getPlayerWorld(id) 
                && virtualWorld == getPlayerVirtualWorld(id))
            {
                base.send(id, relability);
            }
        }
    }
}