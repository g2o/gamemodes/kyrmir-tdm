
setTimer(Effect.Manager.checkEffect.bindenv(Effect.Manager), 500, 0);

addEventHandler("onPlayerEquipHandItem", function(playerId, hand, itemId) 
{
    getEffectManager().playerEquipHandItem(playerId, itemId);
});

addEventHandler("Damage.onPlayerDead", function(playerId, killerId, hitList)
{
    getEffectManager().playerDead(playerId);
});