
class Npc.Grid
{
    _world = null;

    _cellSize = 0;
    _gridSize = null;

    _gridSquareCords = null;
    _grid = null;

    constructor(world, x_start, y_start, x_end, y_end, cellSize = 2000)
    {   
        local calcRow = (abs(y_start) + y_end);
        local calcColumn = (abs(x_start) + x_end);
        
        local column = calcColumn / cellSize;
        local row = calcRow / cellSize;

        _world = world;
        _cellSize = cellSize.tofloat();

        if((calcColumn % cellSize && calcRow % cellSize) == 0 && (column > 1 && row > 1))
        {
            _gridSize = { column = column, row = row };

            _gridSquareCords = { x1 = x_start, y1 = y_start, x2 = x_end, y2 = y_end };
            _grid = generateGrid();
        }
    }

//:protected
    function generateGrid()
    {
        local _grid = [];
        
        local columnNumber = _gridSize.column;
        local rowNumber = _gridSize.row;

        for(local i = 0; i < rowNumber; i++)
        {
            local row = [];
            
            for(local j = 0; j < columnNumber; j++)
                row.push(Npc.GridCell(j, i));

            _grid.push(row);
        }
    
        return _grid;
    }

#public
    function getNearestCell(pos_x, pos_y)
    {
        local column = ceil(abs(_gridSquareCords.x1 + pos_x) / _cellSize) - 1;
        local row = ceil(abs(_gridSquareCords.y1 + pos_y) / _cellSize) - 1;
        
        if((column >= 0 && column < _gridSize.column) 
            && (row >= 0 && row < _gridSize.row))
        {                
            return _grid[row][column];
        }

        return null;
    }

    function getInRangeCells(column, row)
    {
        local cells = [];

        for (local i = row - 1; i <= row + 1; i++)
        {
            if (i < 0 || i >= _gridSize.row) continue;

            for (local j = column - 1; j <= column + 1; j++)
            {
                if (j < 0 || j >= _gridSize.column) continue;

                cells.push(_grid[i][j]);
            }
        }
        
        return cells;
    }

    function getWorld() { return _world; }
}