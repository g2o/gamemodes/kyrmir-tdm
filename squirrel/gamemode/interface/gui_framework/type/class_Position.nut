
class GUI.Position
{
	_x = 0;
	_y = 0;
	
	constructor(x, y)
	{
        _x = x;
		_y = y;
	}

//:public
	function setPosition(x, y) 
    {
        _x = x;
        _y = y;      
    }

	function getPosition() { return { x = _x, y = _y }; }

	function setX(x)
	{
		_x = x;
	}

	function getX() { return _x; }

	function setY(y)
	{
		_y = y;
	}

	function getY() { return _y; }

	function setPositionPx(x, y)
	{
        _x = anx(x);
        _y = any(y);  
	}

	function getPositionPx() { return { x = nax(_x), y = nay(_y) }}

	function setXPx(x)
	{
		_x = anx(x);
	}

	function getXPx() { return nax(_x); }

	function setYPx(y)
	{
		_y = any(y);
	}

	function getYPx() { return nay(_y); }
	
	function setPositionPr(x, y)
	{
 		local resolution = getResolution();

		_x = anx(x / 100.0 * resolution.x);
		_y = any(y / 100.0 * resolution.y);
	}
	
	function getPositionPr()
	{
 			local resolution = getResolution();
        return { x = (_x / anx(resolution.x).tofloat()) * 100, y = (_y / any(resolution.y).tofloat()) * 100 }
	}

	function setXPr(x)
	{
		_x = anx(x / 100.0 * getResolution().x);
	}

	function getXPr()
	{
		return (_x / anx(getResolution().x).tofloat()) * 100;
	}

	function setYPr(y)
	{
		_y = any(y / 100.0 * getResolution().y);
	}

	function getYPr()
	{
		return (_y / any(getResolution().y).tofloat()) * 100;
	}
}

class GUI.PositionPx extends GUI.Position
{
	constructor(x, y)
	{
        base.constructor(anx(x), any(y));
	}
}

class GUI.PositionPr extends GUI.Position
{
	constructor(x, y)
	{
			local resolution = getResolution();  
		base.constructor(anx(x / 100.0 * resolution.x), any(y / 100.0 * resolution.y));
	}
}