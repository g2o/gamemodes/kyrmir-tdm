
local widthMultipler = getResolution().x.tofloat() / getResolution().y.tofloat();

class GUI.Square
{
	_size = 0;

	constructor(size)
	{
	    _size = size; 
	}
    
//:public
	function setSize(size) 
    {
        _size = width;                
    }

	function getSize() { return { width = _size, height = _size }; }

	function getWidth() { return _size; }

	function getHeight() { return _size; }

	function setSizePx(width, height)
	{
        _width = anx(width);
        _height = any(height);          
    }

	function getSizePx() { return { width = nax(_size), height = nay(_size) } }

	function getWidthPx() { return nax(_width); }

	function getHeightPx() { return nay(_height); }

	function setSizePr(width, height)
	{
 		local resolution = getResolution();
	
	    _width = anx(width / 100.0 * resolution.x);
	    _height = any(height / 100.0 * resolution.y);
	}
	
	function getSizePr()
	{
 			local resolution = getResolution();
        return { width = (_width / anx(resolution.x).tofloat()) * 100, height = (_height / any(resolution.y).tofloat()) * 100 }
	}

	function getWidthPr() { 
		return (_width / anx(getResolution().x).tofloat()) * 100; 
	}

	function getHeightPr() { return (_height / any(getResolution().y).tofloat()) * 100; }
}

class GUI.SquarePx extends GUI.Square
{
	constructor(width, height)
	{
	    base.constructor(anx(width), any(height));
	}
}

class GUI.SquarePr extends GUI.Square
{
	constructor(width, height)
	{
			local resolution = getResolution();
		base.constructor(anx(width / 100.0 * resolution.x), any(height / 100.0 * resolution.y));
	}
}