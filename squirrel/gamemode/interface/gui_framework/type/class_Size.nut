
class GUI.Size
{
	_width = 0;
    _height = 0;

	constructor(width, height)
	{
	    _width = width; 
		_height = height;
	}
    
//:public
	function setSize(width, height) 
    {
        _width = width;                
        _height = height;
    }

	function getSize() { return { width = _width, height = _height }; }

	function setWidth(width)
	{ 
        _width = width; 
	}

	function getWidth() { return _width; }

	function setHeight(height)
	{
        _height = height;
	}

	function getHeight() { return _height; }

	function setSizePx(width, height)
	{
        _width = anx(width);
        _height = any(height);          
    }

	function getSizePx() { return { width = nax(_width), height = nay(_height) } }

	function setWidthPx(width)
	{
	    _width = anx(width);
	}

	function getWidthPx() { return nax(_width); }

	function setHeightPx(height)
	{
	    _height = any(height);  
	}

	function getHeightPx() { return nay(_height); }

	function setSizePr(width, height)
	{
 		local resolution = getResolution();
	
	    _width = anx(width / 100.0 * resolution.x);
	    _height = any(height / 100.0 * resolution.y);
	}
	
	function getSizePr()
	{
 			local resolution = getResolution();
        return { width = (_width / anx(resolution.x).tofloat()) * 100, height = (_height / any(resolution.y).tofloat()) * 100 }
	}

	function setWidthPr(width)
	{
	    _width = anx(width / 100.0 * getResolution().x);
	}

	function getWidthPr() { 
		return (_width / anx(getResolution().x).tofloat()) * 100; 
	}

	function setHeightPr(height)
	{
	    _height = any(height / 100.0 * getResolution().y);
	}

	function getHeightPr() { return (_height / any(getResolution().y).tofloat()) * 100; }
}

class GUI.SizePx extends GUI.Size
{
	constructor(width, height)
	{
	    base.constructor(anx(width), any(height));
	}
}

class GUI.SizePr extends GUI.Size
{
	constructor(width, height)
	{
			local resolution = getResolution();
		base.constructor(anx(width / 100.0 * resolution.x), any(height / 100.0 * resolution.y));
	}
}