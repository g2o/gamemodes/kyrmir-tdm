
class GUI.List extends GUI.Event
{
	_position = null;

    _lineLimit = 0;
    _lineGap = 0;
    
	_lineSize = null;

	_scrollable = false;
	_visible = false;

	_currentButton = 0;
    _focused = false;

    _buttonList = null;

	constructor(position, lineLimit = 0)
	{
		base.constructor();
	
		_position = position;

		_lineLimit = lineLimit;
		_lineGap = 0;

        _lineSize = GUI.SizePr(10, 5);

		_scrollable = false;
		_visible = false;
		
		_currentButton = 0;
   	 	_focused = false;

		_buttonList = [];
	}

//:public
	function destroy()
	{
		setVisible(false);
			clearButtons();
		
		_buttonList = null;
			base.destroy();
	}

	function setScrollable(scroll) 
	{
		_scrollable = scroll;
			return this;
	}

	function isScrollable() { return _scrollable; }

	function setVisible(visible)
	{
		if(visible != _visible)
		{
			if(visible)
			{
				local buttonLimit = _lineLimit ? (_lineLimit < _buttonList.len() ? _lineLimit : _buttonList.len()) : _buttonList.len();
				
				if(_buttonList.len() > 0 )
				{
					local position = _position.getPosition();

					for(local i = 0; i < buttonLimit; i++)
					{
							local button = _buttonList[i];
						button.setPosition(GUI.Position(position.x, position.y + i * (_lineSize.getHeight() + _lineGap))).setVisible(true);

						if(button instanceof GUI.ListText)
							button.setSize(_lineSize);
					}

					_buttonList[_currentButton].setFocus(true);
				}

				getSceneManager().pushObject(this);					
			}
			else
			{
				_currentButton = 0;

				foreach(button in _buttonList)
					button.setVisible(false);	
		
				_focused = false;

				getSceneManager().removeObject(this);	
			}
			
			callEvent(GUIEventInterface.Visible, _visible = visible);
		}

		return this;
	}

	function setCurrentButton(currentButton) 
	{
		foreach(button in _buttonList)
		{
			if(button != currentButton)
				button.setFocus(false);
		}
		
		if(typeof(currentButton) == "instance")
		{
			local focusId = _buttonList.find(currentButton);
			
			if(focusId != null)
			{ 
				_currentButton = focusId;
			}
		}
		
		callEvent(GUIEventInterface.FocusElement, _currentButton);
	}

	function getCurrentButton() { return _currentButton; }

	function mouseClick()
	{
		return callEvent(GUIEventInterface.Click, _currentButton);
	}

	function setPosition(position)
	{
		local position = (_position = position).getPosition();

		if(_visible) 	
		{
			local i = 0;

            foreach(button in _buttonList)
			{				
     		    if(button.isVisible())
                	button.setPosition(GUI.Position(position.x, position.y + i++ * (_lineSize.getHeight() + _lineGap)));
			}
		}
		
		return this;
	}

    function getPosition() { return _position; }

    function setLineLimit(line)
    {
		_lineLimit = line
			return this;
	}

    function getLineLimit() { return _lineLimit; }

    function setLineSize(size)
	{
		_lineSize = size;

		if(_visible)
		{
			local position = _position.getPosition(), i = 0;

            foreach(button in _buttonList)
			{
     		    if(button.isVisible())
                	button.setPosition(GUI.Position(position.x, position.y + i++ * (_lineSize.getHeight() + _lineGap)));
			}
		}
		else 
		{
			foreach(button in _buttonList)
			{
				if(button instanceof GUI.ListText)
					button.setSize(_lineSize);
			}
		}

		return this;
	}

	function getLineSize() { return _lineSize; }

	function setLineGap(lineGap)
	{
		_lineGap = lineGap;
			return this;
	}

	function getLineGap() { return _lineGap; }

	function pushListButton(button, position = -1)
	{
		if(position == -1) position = _buttonList.len();

		button.setList(this)
		
		if(button instanceof GUI.ListText)
		{
			button.setSize(_lineSize);
		}

		_buttonList.insert(position, button);
		
		if(_visible)
		{
			local lastLine = _currentButton + _lineLimit;

			if(position <= lastLine)
			{		
				local position = _position.getPosition();

				foreach(index, button in _buttonList)
				{
					if(index >= _currentButton && index < lastLine)
					{
						button.setPosition(GUI.Position(position.x, position.y + (index - _currentButton) * (_lineSize.getHeight() + _lineGap))).setVisible(true);
					}
					else button.setVisible(false);
				}	
			}
		}

		return this;
	}

	function removeListButton(button)
	{
		if(typeof(button) == "integer")
		{
			if(button < button.len())
			{
				button = _buttonList[button];
			}
		}			
	
		local id = _buttonList.find(button);
		
		if(id != null)
		{
			button.setVisible(false).setList(null);
			_buttonList.remove(id);
		}

		return this;
	}

	function clearButtons()
	{
		_currentButton = 0;

		foreach(button in _buttonList)
			button.destroy();

		_buttonList.clear();

		return this;
	}

	function getButtons() { return _buttonList; }

//:protected
	function up()
	{
		local arraySize = _buttonList.len();

		if(arraySize > 1)
		{
			_buttonList[_currentButton].setFocus(false);

			local prevButton = _currentButton - 1;

			if(_lineLimit != 0)
			{
				if(prevButton < 0)
				{
					local disparity = arraySize - (_lineLimit > arraySize ? arraySize : _lineLimit);
					
					local position = _position.getPosition();
					
					foreach(i, button in _buttonList)
					{
						if(i >= disparity)
							button.setPosition(GUI.Position(position.x, position.y + (i - disparity) * (_lineSize.getHeight() + _lineGap))).setVisible(true);
						else 
							button.setVisible(false);
					}

					prevButton = arraySize - 1;
				}
				else if(!_buttonList[prevButton].isVisible()) 
				{
					local realMax = _lineLimit - 1;
					local suma = prevButton + realMax;

					_buttonList[suma + 1].setVisible(false);

					for(local i = _currentButton; i <= suma; i++)
					{
						local position = _buttonList[i].getPosition().getPosition();
						_buttonList[i].setPosition(GUI.Position(position.x, position.y += (_lineSize.getHeight() + _lineGap)));
					}

					_buttonList[prevButton].setPosition(_position).setVisible(true);
				}
			}

			_buttonList[_currentButton = prevButton].setFocus(true);	
		}
	}

	function down()
	{
		local arraySize = _buttonList.len();

		if(arraySize > 1)
		{
			_buttonList[_currentButton].setFocus(false);

			local nextButton = _currentButton + 1;

			if(_lineLimit != 0)
			{			
				if(nextButton >= arraySize)
				{
					local position = _position.getPosition();
		
					foreach(i, button in _buttonList)
					{
						if(i < _lineLimit)
							button.setPosition(GUI.Position(position.x, position.y + i * (_lineSize.getHeight() + _lineGap))).setVisible(true);
						else 
							button.setVisible(false);
					}

					nextButton = 0;			
				}
				else if(!_buttonList[nextButton].isVisible())
				{
					local realMax = _lineLimit - 1;

					local disparity = nextButton - realMax;
					local suma = disparity + realMax;
				
					_buttonList[disparity - 1].setVisible(false);

					for(local i = disparity; i <= suma; i++)
					{
						local position = _buttonList[i].getPosition().getPosition();
						_buttonList[i].setPosition(GUI.Position(position.x, position.y -= (_lineSize.getHeight() + _lineGap)));
					}

					local position = _position.getPosition();

					_buttonList[suma]
					.setPosition(GUI.Position(position.x, position.y + realMax * (_lineSize.getHeight() + _lineGap)))
					.setVisible(true);
				}
			}

			_buttonList[_currentButton = nextButton].setFocus(true);
		}
	}

	function returnKey()
	{
		if(_buttonList[_currentButton].clickFromList() == false)
		{
			return callEvent(GUIEventInterface.Click, _currentButton);
		}

		return true;
	}
	
	function mouseWheel(direction)
	{
		if(_scrollable && _focused)
		{
			switch(direction)
			{
				case 1: up(); break;
				case -1: down(); break;
			}
		}
	}

//:public
	function keyBoard(key)
	{
		switch(key)
		{
			case KEY_UP: 
			case KEY_W:
				up(); 
			break;
			case KEY_DOWN: 
			case KEY_S: 
				down(); 
			break;
			case KEY_RETURN: return returnKey(); break;
		}
	}

	function setFocus(focus)
	{
		if(_visible)
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
		}

		return this;
	}

	function isFocused() { return _focused; }

	function cursor(cursorPosition)
	{
		local size = _lineSize.getSize();
		local position = _position.getPosition();

		if(GUI.isCursorIn(position.x, position.y, size.width, (_lineLimit != 0 ? _lineLimit : _buttonList.len()) * (size.height + _lineGap), cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
	}	
}
