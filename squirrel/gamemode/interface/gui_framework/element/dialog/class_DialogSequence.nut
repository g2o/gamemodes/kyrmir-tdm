
class GUI.DialogSequence extends GUI.Event
{
    _current_dialogList = 0;
    _time = 0;

    _sound = null;
    _start = false;
    
    _dialogList = null;

	constructor()
	{
        base.constructor();

        _current_dialogList = 0;
        _time = 0;

        _sound = null;
        _start = false;
    
        _dialogList = [];
	}

//:private
    function _camera(speakerId)
    {
        try 
        {
            local angle = (getPlayerAngle(speakerId) + 135) / 180 * PI,
                position = getPlayerPosition(speakerId);

            Camera.setPosition(position.x + sin(angle) * 150, position.y + 100, position.z + cos(angle) * 150); 
            Camera.setRotation(0, getPlayerAngle(speakerId) - 35, 0);
        }
        catch(error)
        {
            print("_camera - " + error);
        }
    }

//:public
	function destroy()
	{
        foreach(dialog in _dialogList)
            dialog.destroy();
        
        _dialogList = null;
		    base.destroy();
	}

    function addText(dialog)
    {
        _dialogList.push(dialog);
            return this;
    }

    function getDialogLists() { return _dialogList; }

    function start() 
    {
        if(!_start)
        {
            if(_dialogList.len() != 0)
            {
                local dialog = _dialogList[_current_dialogList];
                local sound = dialog.getSound(); 

                _camera(dialog.getSpeaker());

                if(sound)
                {
                    _sound = Sound(sound);
                    _sound.play();

                    _time = _sound.playingTime;
                }   
                else _time = dialog.getTimestamp();

                _time += getTickCount();

                dialog.setVisible(_start = true); 
                    callEvent(GUIEventDialog.Start);
                getSceneManager().pushObject(this);		
            }
            else 
            {
                    callEvent(GUIEventDialog.Stop);
                getSceneManager().removeObject(this);	
            }
        }
    }

    function stop() 
    {
        if(_start)
        {
            _dialogList[_current_dialogList].setVisible(_start = false);
            
            _current_dialogList = 0;
            _time = 0;
            
            _camera(heroId);

                callEvent(GUIEventDialog.Stop);
            getSceneManager().removeObject(this);				
        }
    }

    function next() 
    {
        if(_start)
        {
            local next_dialogList = _current_dialogList + 1;    

            callEvent(GUIEventDialog.Next, next_dialogList);
            
            if(next_dialogList < _dialogList.len())
            {
                if(_sound) 
                {
                    _sound.stop();
                    _sound = null;
                }

                local dialog = _dialogList[next_dialogList];
                local sound = dialog.getSound();    

                _camera(dialog.getSpeaker());

                if(sound)
                {
                    _sound = Sound(sound);
                    _sound.play();

                    _time = _sound.playingTime;
                }   
                else _time = dialog.getTimestamp();

                _time += getTickCount();

                _dialogList[_current_dialogList].setVisible(false);
                _dialogList[_current_dialogList = next_dialogList].setVisible(true);
            }
            else stop();
        }
    }

    function isStarted() { return _start; }
    
	function keyBoard(key)
	{
		if(!isConsoleOpen() && key == KEY_SPACE)
		{					
                next();
            cancelEvent();
		}
	}

    function refresh()
    {
        if(_time < getTickCount()) next();
    }
}
