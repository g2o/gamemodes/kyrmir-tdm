
class GUI.Draw extends GUI.Event 
{
	_pureText = false;
	_centerAtPosition = false;

    _text = null;
    _fontFile = "FONT_OLD_10_WHITE_HI";

	_color = null;
	_focusColor = null;

	_alpha = -1;
	_focusAlpha = -1;

	_textScale = null;

    _draw = null;

    _position = null;

    _visible = false;
    _focused = false;

	_disableFocus = false;

    constructor(text, position = null)
    {
        base.constructor();

		_pureText = false;
		_centerAtPosition = false;

        _text = text;
    	_fontFile = "FONT_OLD_10_WHITE_HI";
 			
        _color = null;
		_focusColor = null;    
		
		_textScale = { width = 1.0, height = 1.0 };

        _alpha = -1;
		_focusAlpha = -1;

		_draw = null;

        _position = position ? position : GUI.Position(0, 0);

        _visible = false;
		_focused = false;
		
		_disableFocus = false;
    }

//:public
	function destroy()
	{
		setVisible(false);
			base.destroy();
	}

	function setPureText(pureText)
	{
		_pureText = pureText;
			return this;
	}	

	function isPureTextEnabled() { return _pureText; }
	
	function setText(text)
	{
		if(_text != text)
		{
			_text = text;

			if(_visible)
			{
				_draw.text = _pureText ? text : getTranslation(text);
			
				if(_centerAtPosition)
				{
						local position = _position.getPosition();
					_draw.setPosition(position.x - _draw.width / 2, position.y);
				}
			}
		}
		
		return this;
	}

	function refreshText()
	{
		if(_visible)
		{
			_draw.font = getLanguageFont(_fontFile);
			_draw.text = _pureText ? _text : getTranslation(_text);

			if(_centerAtPosition)
			{
					local position = _position.getPosition();
				_draw.setPosition(position.x - _draw.width / 2, position.y);
			}
		}
	}

	function setCenterAtPosition(centerAtPosition)
	{
		_centerAtPosition = centerAtPosition;

		if(_visible)
		{
				local position = _position.getPosition();
			_draw.setPosition(position.x - _draw.width / 2, position.y);
		}

		return this;
	}

    function getText() { return _text; } 

	function setFont(font)
	{
		_fontFile = font;

		if(_visible)
			_draw.font = getLanguageFont(font);

		return this;
	}

	function getFont() { return _fontFile; }
	
	function setColor(color)
	{
		if(color)
		{
			local color = (_color = color).getColor();

			if(_visible && !_focused)
				_draw.setColor(color.r, color.g, color.b);
		}

		return this;
	}

	function getColor() { return _color; }

	function setFocusColor(color)
	{
		local color = (_focusColor = color).getColor();
		
		if(_visible && _focused)
			_draw.setColor(color.r, color.g, color.b);

		return this;
	}

	function getFocusColor() { return _focusColor; }

	function setAlpha(alpha)
	{
		_alpha = alpha;

		if(_visible && !_focused)
			_draw.alpha = alpha;
		
		return this;
	}

	function getAlpha() { return _alpha; }

	function setFocusAlpha(alpha)
	{
		_focusAlpha = alpha;

		if(_visible && _focused)
			_draw.alpha = alpha;

		return this;
	}

	function getFocusAlpha() { return _focusAlpha; }
	
	function setTextScale(width, height)
	{
		_textScale.width = width;
		_textScale.height = height;
		
		if(_draw)
			_draw.setScale(width, height)
		
		return this;
	}

	function getTextScale() { return _textScale; }

	function setPosition(position)
	{
		local position = (_position = position).getPosition();

		if(_visible) 	
		{
			_draw.setPosition(position.x, position.y);
		
			if(_centerAtPosition)
				_draw.setPosition(position.x - _draw.width / 2, position.y);
		}

		return this;
	}

    function getPosition() { return _position; }

	function setVisible(visible)
	{
		if(visible != _visible)
		{
			if(visible)
			{
				local position = _position.getPosition();

				_draw = Draw(position.x, position.y, _pureText ? _text : getTranslation(_text));
				
				if(_color)
				{
					local color = _color.getColor();		
						_draw.setColor(color.r, color.g, color.b);
				}
				else _draw.setColor(200, 184, 152);
				
				if(_alpha != -1)
					_draw.alpha = _alpha;
				
				_draw.font = getLanguageFont(_fontFile);

				_draw.setScale(_textScale.width, _textScale.height);
				
				if(_centerAtPosition)
					_draw.setPosition(position.x - _draw.width / 2, position.y);

				_draw.visible = true;
				
				getSceneManager().pushObject(this);				
			}
			else 
			{	
				_focused = false;
			
				_draw.visible = false;
				_draw = null;

				getSceneManager().removeObject(this);					
			}
			
			callEvent(GUIEventInterface.Visible, _visible = visible);
		}
	
		return this;
	}

    function isVisible() { return _visible; }

//:private
	function setFocus_Graphical(focus)
	{
		if(focus)
		{			
			if(_focusColor)
			{
				local color = _focusColor.getColor();
					_draw.setColor(color.r, color.g, color.b);
			}

			if(_focusAlpha != -1)
				_draw.alpha = _focusAlpha;
		}
		else 
		{
			if(_color)
			{
				local color = _color.getColor();
					_draw.setColor(color.r, color.g, color.b);
			}

			if(_alpha != -1)
				_draw.alpha = _alpha;			
		}
	}

//:public
	function setDisableFocus(disableFocus)
	{
		_disableFocus = disableFocus;
			return this;
	}

	function isFocusDiasbled() { return _disableFocus; }

	function setFocus(focus)
	{
		if(_visible)
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
			
			setFocus_Graphical(focus);
		}
	}

	function isFocused() { return _focused; }
	
	function cursor(cursorPosition)
	{
		if(_disableFocus) return;
		
		local position = _position.getPosition();

		if(GUI.isCursorIn(position.x, position.y, _draw.width, _draw.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
	}
}