
class GUI.GameText 
{
    _draw = null;

    _animationInterval = 0;
    _animationBeginTime = null;

    _deltaTime = 0.0;

    constructor(position, text, fontFile, timestamp = 5000.0, centerAt = false, r = 200, g = 184, b = 152, pureText = false)
    {
        local position = position.getPosition();

        _draw = Draw(position.x, position.y, pureText ? text : getTranslation(text));
        _draw.font = getLanguageFont(fontFile);

        if(centerAt)
            _draw.setPosition(position.x - _draw.width / 2, position.y);
        
        _draw.setColor(r, g, b);
        _draw.visible = true;

        _animationInterval = timestamp;
        _animationBeginTime = getTickCount();       

        getSceneManager().pushObject(this);				
    }

//:public
    function isVisible() { return _draw != null; }

    function getDeltaTime() { return _deltaTime; }
    
    function render()
    {
        if(_animationInterval <= 0) return;

        if(_animationBeginTime != -1)
        {
            _deltaTime = (getTickCount() - _animationBeginTime) / _animationInterval;

            if(_deltaTime < 1.0)
            {
                local position = _draw.getPosition();

                _draw.setPosition(position.x, position.y - 15);
                _draw.alpha = 255 * (1.0 - _deltaTime);
            }
            else
            {
                _draw.alpha = 255;
                _draw.visible = false;
                
                _draw = null;

                _animationBeginTime = -1;
                
                getSceneManager().removeObject(this);
            }
        }
    }
}