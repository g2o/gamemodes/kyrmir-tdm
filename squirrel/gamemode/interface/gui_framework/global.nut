
enum GUITextFormat 
{
    Center,
	Left,
	Right
} 

enum GUIEventInterface 
{
	Default,
	Visible,
	Focus,
	FocusElement,
	Active,
	Click,
	Input
}

enum GUIInputType 
{
	Text,
	Number
}

enum GUIEventDialog 
{
	Start,
	Stop,
	Next
}

enum MouseButton 
{
	Press,
	Release
}