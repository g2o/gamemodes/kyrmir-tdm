
local quickSlots = {};

local renderSize = 4;
local widthMultipler = getResolution().x.tofloat() / getResolution().y.tofloat();

local listBottomGapSize = 14,
    listTopGapSize = 5;

local eqViewVisible = false,
    interfaceVisible = true;

local lastWeaponId = -1;
local lastWeaponTick = null;

// LOCAL FUNCTIONS
local function updateQuickSlotPosition()
{
    local qsSize = quickSlots.len();
    local width = (qsSize * renderSize) + ((qsSize - 1) * (renderSize / 2));
    
    local startPositionX = 50.0 - width / 2.0,
        startPositionY = (100 - (eqViewVisible ? (100 - listTopGapSize) : listBottomGapSize));
    
    local eIndex = 0;

    foreach(index in SLOT_ORDER)
    {
        if(index in quickSlots)
            quickSlots[index].setPosition(GUI.PositionPr(startPositionX + (renderSize + (renderSize / 2)) * eIndex++, startPositionY));
    }
}

local function useQuickSlotKey(keyId)
{
    local tick = getTickCount();

    if(lastWeaponTick > tick)
        return;

    local item = getItemBySlot(keyId);

    if(item != null)
    {
        local template = item.getTemplate(), templateId = template.getId();

        if(template.isUsable())
        {
            if(useItem(heroId, templateId) == false) 
            {
                GUI.GameText(GUI.PositionPr(50, 40), "USE_ONLY_DURING_S_RUN", "FONT_OLD_20_WHITE_HI", 2500.0, true, 190, 15, 15);
            }
        } 
        else 
        {
            if(template.isSpell())
            {
                if(readySpell(heroId, templateId))
                    lastWeaponId = templateId;
                else
                    unreadySpell(heroId);
            }
            else 
            {
                if(drawWeapon(heroId, templateId))
                    lastWeaponId = templateId;
                else
                    removeWeapon(heroId);
            }
        }
    }

    lastWeaponTick = tick + 350;   
}

local function specialInstancesWeaponMode()
{
    if(isPlayerDead(heroId)) return false;

    local instance = getPlayerInstance(heroId);

    if(instance == "ALEXIA_MIDGAR")
    {
        setPlayerWeaponMode(heroId, WEAPONMODE_2HS);
        return true;
    }
    else 
    {
        if(instance != "PC_HERO")
        {
            setPlayerWeaponMode(heroId, WEAPONMODE_FIST);
            return true;
        }
    }

    return false; 
}

local function useLastWeapon()
{
    local tick = getTickCount();

    if(lastWeaponTick > tick )
        return;
    
    local weaponMode = getPlayerWeaponMode(heroId);
    
    if(weaponMode == WEAPONMODE_NONE)
    {
        if(specialInstancesWeaponMode())
            return;

        if(isItemEquipped(lastWeaponId) == false)
        {
            lastWeaponId = -1;
        }
            
        if(lastWeaponId != -1)
        {
            local item = hasItem(lastWeaponId);

            if(item.getTemplate().isSpell())
            {
                readySpell(heroId, lastWeaponId);
            }
            else
                drawWeapon(heroId, lastWeaponId);
        }
        else 
        {
            local equipment = getHero().getEquipmentModule();

            local meleeWeapon = equipment.getMeleeWeapon(), 
                rangedWeapon = equipment.getRangedWeapon();

            if(meleeWeapon != null)
            {
                drawWeapon(heroId, lastWeaponId = meleeWeapon.getTemplate().getId());
            }
            else if(rangedWeapon != null)
            {
                drawWeapon(heroId, lastWeaponId = rangedWeapon.getTemplate().getId());
            }
            else 
                _drawWeapon(heroId, WEAPONMODE_FIST);
        }
    }
    else
    {
        if(getPlayerInstance(heroId) != "PC_HERO")
        {
            setPlayerWeaponMode(heroId, WEAPONMODE_NONE);
            return;
        }

        if(weaponMode == WEAPONMODE_FIST)
        {
            removeWeapon(heroId);
        }
        else 
        {
            local item = hasItem(lastWeaponId);

            if(item.getTemplate().isSpell())
            {
                unreadySpell(heroId);
            }
            else
                removeWeapon(heroId);
        }
    }
    
    lastWeaponTick = tick + 350;
}

// GLOBAL FUNCTIONS
function qs_checkPositionForEquipment(equipment)
{
    eqViewVisible = equipment;
        updateQuickSlotPosition();
}

function qs_setVisibility(visible)
{
    interfaceVisible = visible;

    foreach(slot in quickSlots)
        slot.setVisible(visible);
}

// EVENTS
addEventHandler("Equipment.onQuickSlotUpdate", function(slotId, item)
{
    if(item != null)
    {
        quickSlots[slotId] <- GUI.Render(item.getTemplate().getVisual(), 
            null, 
            GUI.SizePr(renderSize, renderSize * widthMultipler)
        )
        .setTexture("QUICK_SLOT.TGA")
        .setText(slotId)
        .setVisible(interfaceVisible);        
    }
    else 
    {
        quickSlots[slotId].destroy();
            delete quickSlots[slotId];
    }

    updateQuickSlotPosition();
});

addEventHandler("onKey", function(key)
{
    if(isAnyViewVisible() || isEventCancelled() || isControlsDisabled()) 
        return;
    
    local gameWeapon = getLogicalKeyBinding(GAME_WEAPON);

    if(typeof(gameWeapon) == "integer")
    {
        if(key == getLogicalKeyBinding(GAME_WEAPON)) 
            useLastWeapon(); 
    }
    else 
    {
        foreach(weaponKey in gameWeapon)
        {
            if(key == weaponKey) useLastWeapon();        
        }
    }

    if(getPlayerInstance(heroId) != "PC_HERO")
        return;

    switch(key)
    {
        case KEY_0: useQuickSlotKey(0); break;
        case KEY_1: useQuickSlotKey(1); break;
        case KEY_2: useQuickSlotKey(2); break;
        case KEY_3: useQuickSlotKey(3); break;
        case KEY_4: useQuickSlotKey(4); break;
        case KEY_5: useQuickSlotKey(5); break;
        case KEY_6: useQuickSlotKey(6); break;
        case KEY_7: useQuickSlotKey(7); break;
        case KEY_8: useQuickSlotKey(8); break;
        case KEY_9: useQuickSlotKey(9); break;
    }
});

addEventHandler("View.onViewVisible", function(visible, viewId)
{
    if(viewId != "VIEW_EQUIPMENT")
    {
        qs_setVisibility(!visible);
    }
});

addEventHandler("onPlayerHit", function(killerId, playerId, desc)
{
    if(playerId != heroId) 
        return;
        
    lastWeaponTick = getTickCount() + 350;
});

