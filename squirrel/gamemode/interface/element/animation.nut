
// ANI LIST
local ANIMATIONS = [
    "T_STAND_2_SIT", "T_STAND_2_SLEEPGROUND", "T_STAND_2_PEE", "S_PRAY",
    "T_SEARCH", "T_PLUNDER", "S_LGUARD", "S_HGUARD", "S_DEAD", "S_WASH",
    "R_SCRATCHEGG", "S_FIRE_VICTIM", "T_1HSINSPECT", "T_1HSFREE",
    "T_GREETNOV", "T_WATCHFIGHT_OHNO", "T_YES", "T_NO", "R_SCRATCHHEAD",
    "T_BORINGKICK"
];

// INTERFACE
local animation_View = GUI.View("VIEW_ANIMATION");

local animation_Size = {
    texture = GUI.SizePr(18, 45),
    lineAnimation = GUI.SizePr(16, 2.5)
};

local animation_Position = {
    texture = GUI.PositionPr(78, 5),
    drawTitle = GUI.PositionPr(79, 7),
    listAnimations = GUI.PositionPr(79, 10),
};

local animation_Elements = {
    texture = GUI.Texture("BACKGROUND_1024x2048.TGA", animation_Position.texture, animation_Size.texture),
    drawTitle = GUI.Draw("VIEW_ANIMATION_TITLE", animation_Position.drawTitle),
    listAnimations = GUI.List(animation_Position.listAnimations, 15)
        .setLineSize(animation_Size.lineAnimation)	
        .setScrollable(true)
        .addEvent(GUIEventInterface.Click, function(id) 
        {
            playAni(heroId, ANIMATIONS[id]);
        }),
};

foreach(animation in ANIMATIONS)
{
    animation_Elements.listAnimations.pushListButton(GUI.ListText(animation).setTexture("MENU_INGAME.TGA"));
}

// REGISTER ELEMENTS
animation_View
.pushObject(animation_Elements.texture)
.pushObject(animation_Elements.drawTitle)
.pushObject(animation_Elements.listAnimations);

registerInterfaceView(animation_View, KEY_F6, true);