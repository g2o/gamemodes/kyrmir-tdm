
const FADE_TIME = 700.0;

class ChatInterface.Container 
{
    _visible = false;

    _messageList = null;
    _position = null;

    _lineLimit = 15;
	_letterLimit  = 80;

	_historySize = 50;
	_location = 0;

    _moveLinesBeginTime = -1;

    constructor(position)
    {
        _visible = false;
    
        _messageList = queue();
        _position = position;

        _lineLimit = 15;
        _letterLimit = 80;

        _historySize = 50;
        _location = 0; 
        
        _moveLinesBeginTime = -1;
    }   

//:protected
    function _calcPosition()
    {
        if(_visible == false) return;

        local offset = 0, position = _position.getPosition();

        for(local index = getFirstVisibleLine(), end = getLastVisibleLine(); index <= end; ++index)
        {	
            local message = _messageList[index];
            if(!message.isVisible()) message.setVisible(true);
            
            message.setPosition(position.x, position.y + offset);
            offset += message.getLineHeight();
        }
        
        chatInputSetPosition(position.x, position.y + offset);
    }

//:public
    function setVisible(visible, fade = true)
    {
        if(_messageList.len() == 0 || _visible == visible)
            return;
        
        _visible = visible;

        if(visible)
        {
            local lineLimit = _lineLimit < _messageList.len() ? _lineLimit : _messageList.len(),
                now = getTickCount();
            
            for(local index = getFirstVisibleLine(), end = getLastVisibleLine(); index <= end; ++index)
            {            
                local message = _messageList[index];     

                message.setVisible(true);

                if(fade)
                {
                    message.setAlpha(0);
                    message.setFadeBeginTime(now);
                }
                else 
                    message.setAlpha(255);
            }
        }
        else 
        {
            local now = getTickCount();

            for(local index = getFirstVisibleLine(), end = getLastVisibleLine(); index <= end; ++index)
            {       
                local message = _messageList[index];     

                if(message.isVisible() == false)
                    continue;

                if(fade)
                {
                    message.setAlpha(0);
                    message.setFadeBeginTime(now);
                }
                else 
                    message.setVisible(false);   
            }
        }

        _calcPosition();
    }

    function isVisible() { return _visible; }

    function setMaxLines(lineLimit)
    {
        if (lineLimit <= 0 || lineLimit > 30)
            return;

        for(local i = getFirstVisibleLine(), end = getLastVisibleLine(); i < end; ++i)
            _messageList[i].setVisible(false);
            
        _lineLimit = lineLimit;
        _calcPosition();
    }

    function getLocation() { return _location; }

    function getTopLocation()
    {
        local idx = _messageList.len() - _lineLimit;
        if(idx < 0) idx = 0;
            
        return -idx;
    }

    function getFirstVisibleLine()
    {
        local idx = _messageList.len() - _lineLimit + _location;
        if(idx < 0) idx = 0;
        
        return idx;
    }

    function getLastVisibleLine()
    {
        local idx = _messageList.len() - 1 + _location; 
        if(idx < 0) idx = 0;

        return idx;
    }

    function printMessage(message)
    {
        _messageList.push(message);

        local lastVisibleLine = _messageList[getLastVisibleLine()];

        if(_visible && lastVisibleLine.isVisible() == false)
            lastVisibleLine.setVisible(true);

        local now = getTickCount();

        if(FADE_TIME > 0 && lastVisibleLine.isVisible() == true)
        {
            lastVisibleLine.setAlpha(0);
            lastVisibleLine.setFadeBeginTime(now);
        }

        if(_messageList.len() > _lineLimit)
        {
            _messageList[getFirstVisibleLine() - 1].setVisible(false);
        
            if(_messageList.len() > _historySize)
                _messageList.pop();
            
            if(FADE_TIME > 0)
                _moveLinesBeginTime = now;
            else
                _calcPosition();
        }
        else
            _calcPosition();
    }

    function move(newLocation)
    {
        if (_location == newLocation) return;
        if (_messageList.len() < _lineLimit) return;

        if ((_messageList.len() - _lineLimit + newLocation < 0) || (newLocation > 0))
            return;
        
        for (local i = getFirstVisibleLine(), end = getLastVisibleLine(); i <= end; ++i)
            _messageList[i].setVisible(false);
            
        _location = newLocation;
        _calcPosition();
    }

    function render()
    {           
        local now = getTickCount();

        if(_visible)
        {            
            local deltaTime = (now - _moveLinesBeginTime) / FADE_TIME;
            local firstVisibleLine = getFirstVisibleLine();
           
            local position = _position.getPosition();

            for(local index = 0, end = _messageList.len() - firstVisibleLine; index < end; ++index)
            {
                local message = _messageList[index + firstVisibleLine];

                if(message.isVisible() == false) 
                    continue;

                if(_moveLinesBeginTime != -1)
                {
                    local lineHeight = message.getLineHeight();

                    if(deltaTime < 1.0)
                        message.setPosition(position.x, position.y + lineHeight * (index + 1) - lineHeight * deltaTime);
                    else
                    {
                        _calcPosition();
                        _moveLinesBeginTime = -1;
                    }
                }

                local fadeBeginTime = message.getFadeBeginTime();

                if(fadeBeginTime != -1)
                {
                    local deltaTime = (now - fadeBeginTime) / FADE_TIME;

                    if(deltaTime < 1.0)
                    {
                        message.setAlpha(255 * deltaTime);
                    }
                    else
                    {
                        message.setAlpha(255);
                        message.setFadeBeginTime(-1);
                    }
                }
            }
        }
        else 
        {
            local firstVisibleLine = getFirstVisibleLine();

            for(local index = 0, end = _messageList.len() - firstVisibleLine; index < end; ++index)
            {
                local message = _messageList[index + firstVisibleLine];

                if(message.isVisible() == false) 
                    continue;

                local fadeBeginTime = message.getFadeBeginTime();

                if(fadeBeginTime != -1)
                {
                    local deltaTime = (now - fadeBeginTime) / FADE_TIME;

                    if(deltaTime < 1.0)
                    {
                        message.setAlpha(255 - 255 * deltaTime);
                    }
                    else
                    {
                        message.setAlpha(0);
                        message.setFadeBeginTime(-1);

                        message.setVisible(false);
                    }
                }
            }
        }
    }
}

