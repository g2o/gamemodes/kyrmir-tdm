
local playerList_View = GUI.View("VIEW_PLAYER_LIST");

local playerList_Size = {
    textureBackground = GUI.SizePr(50, 62),
    line = GUI.SizePr(45, 2.5)
}

local playerList_Position = {
    drawTitle = GUI.PositionPr(50, 11),
    textureBackground = GUI.PositionPr(25, 9),
    listPlayers = GUI.PositionPr(27.5, 16),
    drawOnline = GUI.PositionPr(25.5, 73.5) // textureBackground.y + textureBackground.height + 2.5, 
}

local playerList_Elements = {
    drawTitle = GUI.Draw("PLAYER_LIST_VIEW_TITLE", playerList_Position.drawTitle)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setCenterAtPosition(true),
    textureBackground = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", playerList_Position.textureBackground, playerList_Size.textureBackground),
    listPlayers = GUI.List(playerList_Position.listPlayers, 21)
        .setLineSize(playerList_Size.line)	
        .setScrollable(true),
    drawsMembers = {
        "MEMBERS" : GUI.Draw("PLAYER_LIST_VIEW_MEMBERS", GUI.Position(playerList_Position.textureBackground.getX() + playerList_Size.textureBackground.getWidth() + 100, 
            playerList_Position.textureBackground.getY() + 50))
    },
    drawOnline = GUI.Draw("PLAYER_LIST_VIEW_ONLINE;: 0", playerList_Position.drawOnline)
};

// TITLE ROW
playerList_Elements.listPlayers
.pushListButton(GUI.ListRow()
    .pushButton(GUI.Button("PLAYER_LIST_VIEW_ID")
        .setSize(GUI.SizePr(4, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("PLAYER_LIST_VIEW_NICKNAME")
        .setSize(GUI.SizePr(15, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("PLAYER_LIST_VIEW_KILLS")
        .setSize(GUI.SizePr(7, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("PLAYER_LIST_VIEW_DEATHS")
        .setSize(GUI.SizePr(7, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("PLAYER_LIST_VIEW_ASSISTS")
        .setSize(GUI.SizePr(7, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
    .pushButton(GUI.Button("PLAYER_LIST_VIEW_PING")
        .setSize(GUI.SizePr(5, 2.5))
        .setTexture("BACKGROUND_LINE_256X64.TGA")
        .setColor(GUI.Color(255, 255, 255))
    )
);

for(local playerId = 0; playerId < getMaxSlots(); playerId++)
{
    playerList_Elements.listPlayers
    .pushListButton(GUI.ListRow()
        .pushButton(GUI.Button(playerId.tostring())
            .setSize(GUI.SizePr(4, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("SLOT")
            .setSize(GUI.SizePr(15, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(7, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(7, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(7, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
        .pushButton(GUI.Button("-")
            .setSize(GUI.SizePr(5, 2.5))
            .setTexture("BACKGROUND_LINE_256X64.TGA")
            .setPureText(true)
            .setColor(GUI.Color(200, 184, 152))
        )
    );
}

playerList_Elements.drawOnline
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible) 
        playerList_Elements.drawOnline.setText("PLAYER_LIST_VIEW_ONLINE;: " + getPlayersCount());
});

// CALLBACKS
addEventHandler("onPlayerCreate", function(playerId)
{    
    playerList_Elements.drawOnline.setText("PLAYER_LIST_VIEW_ONLINE;: " + getPlayersCount());
    
    local buttons = playerList_Elements.listPlayers.getButtons();
    local color = getPlayerColor(playerId);

    local buttonId = playerId + 1;

    for(local i = 0; i < 6; i++) 
        buttons[buttonId].getColumn(i).setColor(GUI.Color(color.r, color.g, color.b));

    buttons[buttonId].getColumn(1).setText(getPlayerName(playerId));
    
    for(local i = 2; i < 5; i++) 
        buttons[buttonId].getColumn(i).setText("0");
});

addEventHandler("onPlayerDestroy", function(playerId)
{
    playerList_Elements.drawOnline.setText("PLAYER_LIST_VIEW_ONLINE;: " + getPlayersCount());
    
    local buttonId = playerId + 1,
        button = playerList_Elements.listPlayers.getButtons()[buttonId];

    button.getColumn(1).setText("SLOT");

    for(local i = 0; i < 6; i++) 
    {
        button.getColumn(i).setColor(GUI.Color(200, 184, 152));
        if(i > 1) button.getColumn(i).setText("-");
    }
});

addEventHandler("onPlayerChangeNickname", function(playerId, nickname)
{
    playerList_Elements.listPlayers.getButtons()[playerId + 1].getColumn(1).setText(nickname);
});

addEventHandler("onPlayerChangeColor", function(playerId, r, g, b)
{
    for(local i = 0; i < 6; i++) playerList_Elements.listPlayers.getButtons()[playerId + 1].getColumn(i).setColor(GUI.Color(r, g, b));
});

addEventHandler("onPlayerChangePing", function(playerId, ping)
{
    playerList_Elements.listPlayers.getButtons()[playerId + 1].getColumn(5).setText(ping);
});

addEventHandler("Player.onPlayerKills", function(playerId, kills)
{
    playerList_Elements.listPlayers.getButtons()[playerId + 1].getColumn(2).setText(kills);
});

addEventHandler("Player.onPlayerDeaths", function(playerId, deaths)
{
    playerList_Elements.listPlayers.getButtons()[playerId + 1].getColumn(3).setText(deaths);
});

addEventHandler("Player.onPlayerAssists", function(playerId, assists)
{
    playerList_Elements.listPlayers.getButtons()[playerId + 1].getColumn(4).setText(assists);
});

addEventHandler("Player.onPlayerRest", function(playerId) 
{
    local button = playerList_Elements.listPlayers.getButtons()[playerId + 1];

    button.getColumn(2).setText("0");
    button.getColumn(3).setText("0");
    button.getColumn(4).setText("0");
});

addEventHandler("Guild.onInitialData", function()
{
    local x = playerList_Position.textureBackground.getX() + playerList_Size.textureBackground.getWidth() + 100, 
        y = playerList_Position.textureBackground.getY() + 200;

    foreach(guild in getGuilds())
    {
        local guildId = guild.getId(), color = guild.getColor();

        playerList_Elements.drawsMembers[guildId] <- GUI.Draw(guildId + ";: " + guild.getMembers(), GUI.Position(x, y += 200))
            .setColor(GUI.Color(color.r, color.g, color.b))

        playerList_View.pushObject(playerList_Elements.drawsMembers[guildId]);
    }   
});

addEventHandler("Guild.onMembersUpdate", function(guildId, members)
{
    if(isLogged() == false) return;
    
        local guild = getGuildById(guildId);
    playerList_Elements.drawsMembers[guildId].setText(guildId + ";: " + guild.getMembers());
});

addEventHandler("Player.onHeroLogin", function(responseCode)
{
    switch(responseCode)
    {
        case LoginCode.Succes:
        case LoginCode.FirstLogin:
        {   
            local guildId = getPlayerGuild(heroId), guild = getGuildById(guildId);

            if(guild) playerList_Elements.drawsMembers[guildId].setText(guildId + ";: " + guild.getMembers());

                local position = getPlayerPosition(heroId);
            Camera.setPosition(position.x, position.y + 500, position.z);
        }
        break;
    }
});

// REGISTER ELEMENTS
playerList_View
.pushObject(playerList_Elements.textureBackground)
.pushObject(playerList_Elements.drawTitle)
.pushObject(playerList_Elements.listPlayers)
.pushObject(playerList_Elements.drawOnline);

foreach(element in playerList_Elements.drawsMembers)
{
    playerList_View.pushObject(element);
}

registerInterfaceView(playerList_View, KEY_F5, true);