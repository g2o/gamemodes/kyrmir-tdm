
local popupQueue = queue();

local tickTime = null;
local timer = -1;

local roundTheme = Sound("GOTHIC_END_THEME.WAV");

roundTheme.looping = false;
roundTheme.volume = 0.45;

// INTERFACE
local status_View = GUI.View("VIEW_STATUS");

local status_Size = {
    barExperience = GUI.Size(0, 0),
    barParade = GUI.SizePr(15, 2.5),
    textureIcon = GUI.SizePr(2.5, 2.5 * (getResolution().x.tofloat() / getResolution().y.tofloat())) // widthMultipler
};

local status_Position = {
    barExperience = GUI.Position(0, 0),
    barParade = GUI.PositionPr(42.5, 95),
    drawsRound = GUI.Position(100, 4600),
    drawsRound_Status = GUI.Position(100, 4800),
    drawKills = GUI.Position(100, 6500),
    drawDeaths = GUI.Position(100, 6750),
    drawAssists = GUI.Position(100, 7000),
    gameTextLevelPopup = GUI.PositionPr(50, 45),
    gameTextMainPopup = GUI.PositionPr(50, 50),
    drawMultiInfo = GUI.PositionPr(70.5, 1.5),
    textureMonsterStreak = GUI.Position(0, 0),
    texturePlayerStreak = GUI.Position(0, 0),
    texturePlayerGold = GUI.Position(0, 0)
};

local status_Elements = {
    gameTextPopup = null, // NOT REGISTRED IN VIEW...
    barExperience = GUI.Bar(status_Position.barExperience, status_Size.barExperience, GUI.Size(40, 45), null, "BAR_MISC_YELLOW.TGA"), // NOT REGISTRED IN VIEW...
    barParade = GUI.Bar(status_Position.barParade, status_Size.barParade, GUI.Size(40, 45), null, "BAR_MISC_GRAY.TGA")
    drawStatus = GUI.Draw("TIME: 00:00:00\n\nFPS: 0\nPING: 0", GUI.PositionPr(0, 0))
        .setPureText(true),
    drawsRound = {
        "ROUND_TIME" : GUI.Draw("STATUS_VIEW_NO_ROUND", status_Position.drawsRound)
            .setFont("FONT_OLD_10_WHITE_HI")
        "ROUND_BONUS" : GUI.Draw("STATUS_VIEW_BONUS_EMPTY;: 0", status_Position.drawsRound_Status)
            .setFont("FONT_OLD_10_WHITE_HI")
    },
    drawKills = GUI.Draw("STATUS_VIEW_KILLS;: 0", status_Position.drawKills)
        .setFont("FONT_OLD_10_WHITE_HI"),
    drawDeaths = GUI.Draw("STATUS_VIEW_DEATHS;: 0", status_Position.drawDeaths)
        .setFont("FONT_OLD_10_WHITE_HI"),
    drawAssists = GUI.Draw("STATUS_VIEW_ASSISTS;: 0", status_Position.drawAssists)
        .setFont("FONT_OLD_10_WHITE_HI"),  
    drawMultiInfo = GUI.MultiDraw(status_Position.drawMultiInfo, 10)
        .setScrollable(false),
    textureMonsterStreak = GUI.Texture("ICON_STREAK_MONSTER.TGA", status_Position.textureMonsterStreak, status_Size.textureIcon)
    texturePlayerStreak = GUI.Texture("ICON_STREAK_PLAYER.TGA", status_Position.texturePlayerStreak, status_Size.textureIcon)
    texturePlayerGold = GUI.Texture("ICON_GOLD.TGA", status_Position.texturePlayerGold, status_Size.textureIcon)
    buttonMonsterStreak = GUI.Button("0", status_Position.texturePlayerStreak, status_Size.textureIcon)
        .setFont("FONT_OLD_10_WHITE_HI")
        .setPureText(true),
    buttonPlayerStreak = GUI.Button("0", status_Position.texturePlayerStreak, status_Size.textureIcon)
        .setFont("FONT_OLD_10_WHITE_HI")
        .setPureText(true),
    buttonPlayerGold = GUI.Button("0", status_Position.texturePlayerGold, status_Size.textureIcon)
        .setFont("FONT_OLD_10_WHITE_HI")
        .setPureText(true),
};

setHudMode(HUD_MANA_BAR, HUD_MODE_ALWAYS_VISIBLE);

// FUNCTIONS/HOOKS
_setHudMode <- setHudMode;

function setHudMode(typeId, hudMode)
{   
    if(typeId == HUD_ALL)
    {
        switch(hudMode)
        {
            case HUD_MODE_HIDDEN:
                status_Elements.barExperience.setVisible(false);
                status_Elements.barParade.setVisible(false);
            break;
            case HUD_MODE_DEFAULT:
            {
                if(getNextLevelExp() >= 0)
                {
                    status_Elements.barExperience.setVisible(true);
                }
                
                local weaponMode = getPlayerWeaponMode(heroId);

                if(weaponMode == WEAPONMODE_1HS || weaponMode == WEAPONMODE_2HS)
                {
                    status_Elements.barParade.setVisible(true);
                }
            } 
            break;
        }
    }

    _setHudMode(typeId, hudMode);
}

local function pushInfoDraw(text, color, pure)
{   
    local draw = status_Elements.drawMultiInfo;

    if(draw.getSize() > 50) 
    {
        draw.removeDraw(0);
    }

    draw.pushDraw(text, color, -1, null, pure, 0.8, 0.8);
    draw.bottom();
}

// CALLBACKS
addEventHandler("onInit", function()
{
    local barPosition = getBarPosition(HUD_HEALTH_BAR), barSize = getBarSize(HUD_HEALTH_BAR);

    status_Size.barExperience = GUI.Size(barSize.width, barSize.height);
    status_Position.barExperience = GUI.Position(barPosition.x, barPosition.y - barSize.height - any(10));

    status_Elements.barExperience
        .setSize(status_Size.barExperience)
        .setPosition(status_Position.barExperience);
});

addEventHandler("onPlayerCreate", function(playerId)
{ 
    if(isLogged() == false)
        return;
        
    pushInfoDraw(getFormatTranslation("SERVER_MESSAGE_JOIN", getPlayerName(playerId)), GUI.Color(0, 120, 0), true);
});

addEventHandler("onPlayerDestroy", function(playerId)
{ 
    pushInfoDraw(getFormatTranslation("SERVER_MESSAGE_LEFT", getPlayerName(playerId)), GUI.Color(120, 0, 0), true);
});

addEventHandler("Player.onHeroLogin", function(responseCode)
{
    switch(responseCode)
    {
        case LoginCode.Succes:
        case LoginCode.FirstLogin:
        {
            local barPosition = getBarPosition(HUD_MANA_BAR).x + getBarSize(HUD_MANA_BAR).width - 30;
            
            status_Elements.drawStatus.setPosition(GUI.Position(barPosition - 800, 5096));

            local iconWidth = status_Size.textureIcon.getWidth() * 2.5;
            local iconHeight = 94 + status_Size.textureIcon.getHeight();

            local buttonX = barPosition - iconWidth * 1.25;

            // FIRST ICON
            status_Elements.textureMonsterStreak.setPosition(status_Position.textureMonsterStreak = 
                GUI.Position(barPosition - status_Size.textureIcon.getWidth(), 6160));   
            status_Elements.buttonMonsterStreak.setPosition(GUI.Position(buttonX, 6160));   

            // SECOND ICON
            status_Elements.texturePlayerStreak.setPosition(status_Position.texturePlayerStreak = 
                GUI.Position(status_Position.textureMonsterStreak.getX(), iconHeight + status_Position.textureMonsterStreak.getY()));
            status_Elements.buttonPlayerStreak.setPosition(GUI.Position(buttonX, 
                iconHeight + status_Position.textureMonsterStreak.getY()));   

            // THIRD ICON
            status_Elements.texturePlayerGold.setPosition(status_Position.texturePlayerGold = 
                GUI.Position(status_Position.texturePlayerStreak.getX(), iconHeight + status_Position.texturePlayerStreak.getY()));
            status_Elements.buttonPlayerGold.setPosition(GUI.Position(buttonX, 
                iconHeight + status_Position.texturePlayerStreak.getY()));

            local item = hasItem(getItemTemplateIdByInstance("ITMI_GOLD"));
            if(item) status_Elements.buttonPlayerGold.setText(item.getAmount().tostring());

            status_View.setVisible(true);
            status_Elements.barExperience.setProgress(getExp() / getNextLevelExp().tofloat());
        } 
        break;    
    }
});

addEventHandler("Guild.onInitialData", function()
{
    local y = status_Position.drawsRound.getY() + 350;

    foreach(guild in getGuilds())
    {
        local id = guild.getId(), color = guild.getColor();

        status_Elements.drawsRound[id] <- GUI.Draw(id + ";: " + guild.getPoints(), GUI.Position(100, y += 250))
            .setColor(GUI.Color(color.r, color.g, color.b))
            .setFont("FONT_OLD_10_WHITE_HI");

        status_View.pushObject(status_Elements.drawsRound[id]);
    }   
});

addEventHandler("Guild.onPointsUpdate", function(guildId, points)
{
    status_Elements.drawsRound[guildId].setText(guildId + ";: " + points);
});

addEventHandler("Guild.onReset", function(guildId)
{
    status_Elements.drawsRound[guildId].setText(guildId + ";: 0");   
});

addEventHandler("Player.onPlayerKills", function(playerId, kills)
{
    if(playerId != heroId) 
        return;

    status_Elements.drawKills.setText("STATUS_VIEW_KILLS;: " + kills);
});

addEventHandler("Player.onPlayerDeaths", function(playerId, deaths)
{
    if(playerId != heroId) 
        return;
  
    status_Elements.drawDeaths.setText("STATUS_VIEW_DEATHS;: " + deaths);
});

addEventHandler("Player.onPlayerAssists", function(playerId, assists)
{
    if(playerId != heroId) 
        return;
  
    status_Elements.drawAssists.setText("STATUS_VIEW_ASSISTS;: " + assists);
});

addEventHandler("Player.onHeroChangeLevel", function(level, newLevel)
{ 
    if(isLogged() == false) return;

    GUI.GameText(status_Position.gameTextLevelPopup, getFormatTranslation("POP-UP_MSG_LEVEL_UP", newLevel), "FONT_OLD_20_WHITE_HI", 3500.0, true, 200, 184, 152, true);
        Sound("LEVELUP.WAV").play();
    
    addEffect(heroId, "spellFX_Fear")
});

addEventHandler("Player.onHeroChangeExperience", function(experience, newExperience)
{ 
    if(isLogged() == false) return;

    local exp = newExperience > experience ? newExperience - experience : newExperience;
    if(exp > 0) popupQueue.push(getFormatTranslation("POP-UP_MSG_EXP-GAIN", exp));
    
    status_Elements.barExperience.setProgress(newExperience / getNextLevelExp().tofloat());
});

addEventHandler("Player.onHeroChangeExperienceNextLevel", function(experienceNextLevel, newExperienceNextLevel)
{
    if(isLogged() == false) return;

    if(newExperienceNextLevel <= -1) 
    {
        status_Elements.barExperience.setVisible(false);
    }
    else 
        status_Elements.barExperience.setProgress(getExp() / newExperienceNextLevel.tofloat());
});

addEventHandler("Player.onPlayerRest", function(playerId) 
{
    if(playerId != heroId) return;
    
    // RESET LEFT SIDE
    status_Elements.drawKills.setText("STATUS_VIEW_KILLS;: 0");
    status_Elements.drawDeaths.setText("STATUS_VIEW_DEATHS;: 0");
    status_Elements.drawAssists.setText("STATUS_VIEW_ASSISTS;: 0");

    // RESET RIGHT SIDE
    status_Elements.buttonPlayerGold.setText("0");
    status_Elements.barExperience.setProgress(0);
});

addEventHandler("Equipment.onHeroGiveItem", function(itemId, amount)
{
    local instance = getItemTemplateById(itemId).getInstance();

    if(instance == "ITMI_GOLD")
    {
            local item = hasItem(itemId);
        status_Elements.buttonPlayerGold.setText(item.getAmount());
    }

    popupQueue.push(getFormatTranslation("POP-UP_MSG_ITEM-GAIN", instance, amount));
});

addEventHandler("Equipment.onHeroRemoveItem", function(itemId, amount)
{
    if(getItemTemplateById(itemId).getInstance() == "ITMI_GOLD")
    {
            local item = hasItem(itemId);
        status_Elements.buttonPlayerGold.setText(item != null ? item.getAmount() : "0");
    }
});

addEventHandler("onRender", function()
{
    if(isAnyViewVisible() == false) 
    {
        if(popupQueue.len() != 0)
        {  
            local gameText = status_Elements.gameTextPopup;

            if(gameText != null && gameText.getDeltaTime() < 0.30) 
                return;

            status_Elements.gameTextPopup = GUI.GameText(status_Position.gameTextMainPopup, popupQueue.pop(), "FONT_OLD_10_WHITE_HI", 2000.0, true, 230, 230, 230); 
        }
    }

    local draw = status_Elements.drawStatus;

    if(draw.isVisible())
    {
        local tick = getTickCount();

        if(tick > tickTime)
        {
            local currentTime = date();

            local hours = (currentTime.hour < 10 ? "0" : "") + currentTime.hour,
                minutes = (currentTime.min < 10 ? "0" : "") + currentTime.min,
                seconds = (currentTime.sec < 10 ? "0" : "") + currentTime.sec;

            draw.setText(format("TIME: %s:%s:%s\n\nFPS: %d\nPING: %d", hours, minutes, seconds, getFpsRate(), getPlayerPing(heroId)));
        
            tickTime = tick;
        }
    }
});

addEventHandler("Round.onCreateRound", function(round)
{
    local draw = status_Elements.drawsRound["ROUND_TIME"];

    if(round.getBeginingTime() > time())
    {
        timer = setTimer(function() 
        {
            local timeDiff = (round.getBeginingTime() - time());

            if(timeDiff > 0)
                draw.setText("STATUS_VIEW_BEGIN; " + timeDiff + ";s");
            else
            {
                killTimer(timer);
                    timer = -1;
            }
        }, 200, 0);
    }
    else 
    {
            draw.setText("STATUS_VIEW_TARGET;: " + round.getTargetPoints());
        status_Elements.drawsRound["ROUND_BONUS"].setText(getFormatTranslation("STATUS_VIEW_ROUND_BONUS", round.getBonus()));
    }
});

addEventHandler("Round.onRoundStart", function()
{
    if(timer != -1) 
    {
        killTimer(timer);
            timer = -1;
    }

    status_Elements.drawsRound["ROUND_TIME"].setText("STATUS_VIEW_TARGET;: " + getRound().getTargetPoints());
});

addEventHandler("Round.onRoundStop", function()
{
    status_Elements.drawsRound["ROUND_TIME"].setText("STATUS_VIEW_NO_ROUND");

    setInterfaceVisible(true, "VIEW_RANK_LIST"); 
        roundTheme.play();
});

addEventHandler("Round.onRoundBonus", function(bonus)
{
    status_Elements.drawsRound["ROUND_BONUS"].setText(getFormatTranslation("STATUS_VIEW_ROUND_BONUS", bonus));
});

addEventHandler("Round.onRoundTarget", function(points)
{
    status_Elements.drawsRound["ROUND_TIME"].setText("STATUS_VIEW_TARGET;: " + points);
});

addEventHandler("InfoBox.onKill", function(playerId, killerId, streak) 
{
        local color = getPlayerColor(killerId);
    pushInfoDraw(getFormatTranslation("STATUS_VIEW_KILL", getPlayerName(playerId), getPlayerName(killerId), streak), GUI.Color(color.r, color.g, color.b), true);

    if(streak != 0 && (streak % 5) == 0) 
    {
        serverMessage(ALL_CHATS, 128, 0, 0, getFormatTranslation("STATUS_VIEW_STREAK", getPlayerName(killerId), streak, getPlayerName(playerId)));
    }
});

addEventHandler("InfoBox.onThirdLevel", function(playerId) 
{
    serverMessage(ALL_CHATS, 128, 0, 0, getFormatTranslation("STATUS_VIEW_LEVEL_THREE", getPlayerName(playerId)));
});

addEventHandler("InfoBox.onBossDead", function(bossId, killerId) 
{
    serverMessage(ALL_CHATS, 128, 0, 0, getFormatTranslation("STATUS_VIEW_BOSS_KILL", getTranslation(getNpc(bossId).getName()), getPlayerName(killerId)));
});

addEventHandler("InfoBox.onBossRespawn", function(bossId) 
{
    serverMessage(ALL_CHATS, 0, 128, 0, getFormatTranslation("STATUS_VIEW_BOSS_RESPAWN", getTranslation(getNpc(bossId).getName())));
});

addEventHandler("Reward.onHeroMonsterStreak", function(streak) 
{
    status_Elements.buttonMonsterStreak.setText(streak);
});

addEventHandler("Reward.onHeroPlayerStreak", function(streak) 
{
    status_Elements.buttonPlayerStreak.setText(streak);
});

addEventHandler("Area.onHeroEnterArea", function(instance) 
{
    if(instance != getPlayerGuild(heroId)) 
        return;

    if(getPlayerHealth(heroId) != getPlayerMaxHealth(heroId) || getPlayerMana(heroId) != getPlayerMaxMana(heroId))
    {
        GUI.GameText(status_Position.gameTextMainPopup, "STATUS_VIEW_ENTER_CAMP", "FONT_OLD_10_WHITE_HI", 2000.0, true, 0, 128, 0); 
    }
});

addEventHandler("Damage.onParadeUpdate", function(playerId, parade)
{
    if(playerId != heroId) 
        return;

    status_Elements.barParade.setProgress(getPlayerParades(playerId) / MAX_PARADE.tofloat());
});

addEventHandler("onPlayerChangeWeaponMode", function(playerId, oldWeaponMode, newWeaponMode)
{
    if(playerId != heroId) 
        return;

    if(newWeaponMode == WEAPONMODE_1HS || newWeaponMode == WEAPONMODE_2HS)
    {
        status_Elements.barParade.setVisible(true);
    }
    else 
        status_Elements.barParade.setVisible(false);
});

addEventHandler("View.onViewVisible", function(visible, viewId)
{
    status_View.setVisible(!visible);

    if(visible == false)
    {
        status_Elements.drawMultiInfo.bottom();
    }
});

// REGISTER ELEMENTS
status_View
.pushObject(status_Elements.drawStatus)
.pushObject(status_Elements.drawKills)
.pushObject(status_Elements.drawDeaths)
.pushObject(status_Elements.drawAssists)
.pushObject(status_Elements.drawMultiInfo)
.pushObject(status_Elements.textureMonsterStreak)
.pushObject(status_Elements.texturePlayerStreak)
.pushObject(status_Elements.texturePlayerGold)
.pushObject(status_Elements.buttonMonsterStreak)
.pushObject(status_Elements.buttonPlayerStreak)
.pushObject(status_Elements.buttonPlayerGold);

foreach(element in status_Elements.drawsRound)
{
    status_View.pushObject(element);
}
