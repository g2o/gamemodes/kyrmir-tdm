
local help_View = GUI.View("VIEW_HELP");

local help_Size = {
    texture = GUI.SizePr(75, 77),
}

local help_Position = {
    texture = GUI.PositionPr(12.5, 12.5),
    draw_title = GUI.PositionPr(50, 15.5),
    draw_help = GUI.PositionPr(18, 21.5),
}

help_View
.pushObject(GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", help_Position.texture, help_Size.texture))
.pushObject(GUI.Draw("HELP_VIEW_TITLE", help_Position.draw_title)
    .setFont("FONT_OLD_20_WHITE_HI")
    .setColor(GUI.Color(255, 255, 255))
    .setCenterAtPosition(true)
)
.pushObject(GUI.MultiDraw(help_Position.draw_help, 26)
    .pushDraw("HELP_VIEW_RENDER", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_PLAYERLIST", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_ANIMLIST", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_HIDE_UI", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_DEBUG", GUI.Color(255, 255, 255), -1)
    .pushDraw("", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_TALK; / ;HELP_VIEW_SKIP", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_SCROLL", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_STATS", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_MAP", GUI.Color(255, 255, 255), -1)
    .pushDraw("", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_CHAT_CHANGE", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_CHAT_OPEN", GUI.Color(255, 255, 255), -1)
    .pushDraw("", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_QUICKSLOT", GUI.Color(255, 255, 255), -1)
    .pushDraw("", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_VOICE_COMMAND_0;", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_VOICE_COMMAND_1; / ;HELP_VIEW_VOICE_COMMAND_2", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_VOICE_COMMAND_3; / ;HELP_VIEW_VOICE_COMMAND_4", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_VOICE_COMMAND_5; / ;HELP_VIEW_VOICE_COMMAND_6", GUI.Color(255, 255, 255), -1)
    .pushDraw("", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_COMMANDS", GUI.Color(255, 255, 255), -1)
    .pushDraw("HELP_VIEW_COMMANDS_PM", GUI.Color(255, 255, 255), -1)    
    .setScrollable(true)
);

registerInterfaceView(help_View, KEY_H, true);