
addEventHandler("Equipment.onPlayerItemUpdate", Artifact.Manager.playerItemUpdate.bindenv(Artifact.Manager));

addEventHandler("Equipment.onPlayerSpellCast", Artifact.Manager.playerSpellCast.bindenv(Artifact.Manager));

addEventHandler("Damage.onPlayerDead", function(playerId, killerId, hitList)
{
    getArtifactManager().playerDropArtifact(playerId);
});

addEventHandler("Player.onPlayerPreDisconnect", Artifact.Manager.playerDropArtifact.bindenv(Artifact.Manager));

addEventHandler("GroundDrop.onItemVanish", Artifact.Manager.itemVanish.bindenv(Artifact.Manager));

