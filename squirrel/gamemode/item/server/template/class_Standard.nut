
class Item.Standard
{
    static _counter = 
    {
        lastFreeId = 0,
        freeIds = []
    };

    _id = -1;

    _instance = null;
    _visualId = -1;

    _typeId = -1;

    _bounded = false;
    _interactionId = 0;

    constructor(instance, visualId, typeId)
    {
        _id = _generateId();

        _instance = instance;
        _visualId = visualId;

        _typeId = typeId;

        _bounded = false;
        _interactionId = 0;
    }

//:protected
    static function _generateId()
    {
        local id = -1;

        if(_counter.freeIds.len() != 0)
        {
            id = _counter.freeIds[0];
            _counter.freeIds.remove(0);
        }
        else 
            id = _counter.lastFreeId++;

        return id;
    }

    static function _freeId(id)
    {
        _counter.freeIds.push(id);
    }

//:public
    function getId() { return _id; }

    function getInstance() { return _instance; }

    function getVisual() { return Items.name(_visualId); }
   
    function getVisualId() { return _visualId; }

    function getType() { return _typeId; }

    function setBound(bounded) 
    {
        _bounded = bounded;
            return this;
    }

    function isBounded() { return _bounded; }

    function setInteractionType(interaction)
    {
        _interactionId = interaction;
            return this;
    }

    function getInteractionType() { return _interactionId; }

    function isUsable() { return _interactionId == ItemInteraction.Usable; }

    function isEquippable() 
    { 
        return _interactionId == ItemInteraction.Equippable || _interactionId == ItemInteraction.QuickSlot; 
    }

    function isQuickSlotPossible() 
    { 
        return _interactionId == ItemInteraction.Usable || _interactionId == ItemInteraction.QuickSlot;
    }

    function isSpell() { return _typeId == ItemType.Rune || _typeId == ItemType.Scroll; }
}