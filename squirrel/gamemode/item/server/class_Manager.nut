
class Item.Manager 
{
    static _templateList = {};
    static _packetTemplate = null;
    
//:protected 
    static function _listToPacket()
    {
        local packet = Packet(PacketIDs.Item);

        packet.writeInt16(_templateList.len());

        foreach(template in _templateList)
        {
            packet.writeInt16(template.getId());
            packet.writeString(template.getInstance());
            packet.writeInt16(template.getVisualId());

            local typeId = template.getType();

            packet.writeInt8(typeId);
            packet.writeBool(template.isBounded());
            packet.writeInt8(template.getInteractionType());
            
            if(template instanceof Item.Gear)
            {
                packet.writeInt16(template.getRequirement(ItemRequirement.Level));
                packet.writeInt16(template.getRequirement(ItemRequirement.MagicLevel));
                packet.writeInt16(template.getRequirement(ItemRequirement.Strength));
                packet.writeInt16(template.getRequirement(ItemRequirement.Dexterity));   
                packet.writeInt16(template.getRequirement(ItemRequirement.Mana));   
                packet.writeInt16(template.getRequirement(ItemRequirement.Health));  
            }

            switch(template.getclass())
            {
                case Item.Weapon:
                case Item.WeaponRanged:
                {
                    packet.writeInt8(template.getWeaponMode());
                    
                    packet.writeInt8(template.getDamageType());
                    packet.writeInt16(template.getDamage());   

                    if(typeId == ItemType.WeaponRanged)
                    {
                        packet.writeInt16(template.getMunition().getId());
                    }
                } 
                break;
                case Item.Armor: 
                {
                    packet.writeInt16(template.getProtection(DAMAGE_BLUNT));
                    packet.writeInt16(template.getProtection(DAMAGE_EDGE));
                    packet.writeInt16(template.getProtection(DAMAGE_POINT));
                    packet.writeInt16(template.getProtection(DAMAGE_FIRE));
                    packet.writeInt16(template.getProtection(DAMAGE_MAGIC)); 
                } 
                break;
                case Item.Spell: 
                {
                    packet.writeBool(template.isInstant());
                    packet.writeInt8(template.getDamageType());
                    packet.writeInt16(template.getDamage());
                    packet.writeInt16(template.getHealthUsage());
                    packet.writeInt16(template.getManaUsage());
                } 
                break;                
                case Item.Usable:
                {
                    local customEffect = template.getEffect(ItemEffect.Custom);
       
                    packet.writeInt16(template.getEffect(ItemEffect.HealthRecovery));
                    packet.writeInt16(template.getEffect(ItemEffect.HealthPercentRecovery));
                    packet.writeInt16(template.getEffect(ItemEffect.ManaRecovery));
                    packet.writeInt16(template.getEffect(ItemEffect.ManaPercentRecovery));                    
                    packet.writeString(customEffect != 0 ? customEffect : "null");
                } 
                break;    
            }
        }

        return packet;
    }

//:public   
    static function playerJoin(playerId)
    {
        if(_packetTemplate == null) 
            _packetTemplate <- _listToPacket();
        
        _packetTemplate.send(playerId, RELIABLE);
    }

    static function addTemplate(template)
    {
        if(template.getVisualId() != -1)
        {
            _templateList[template.getId()] <- template;
                _packetTemplate <- null;
        } 
        else 
            print("Item.Manager(addTemplate) - Cannot found " + template.getInstance() + " in XML list");
    } 

    static function getTemplateByInstance(instance) 
    { 
        foreach(template in _templateList)
        {
            if(template.getInstance() == instance)
            {
                return template;
            }
        }

        return null;
    }

    static function getTemplateById(itemId)
    {
        return itemId in _templateList ? _templateList[itemId] : null; 
    }

    static function getIdByInstance(instance) 
    { 
        foreach(template in _templateList)
        {
            if(template.getInstance() == instance)
            {
                return template.getId();
            }
        }     
        
        return -1; 
    }

    static function getTemplates() { return _templateList; }
}

getItemManager <- @() Item.Manager; 

// Item
addItemTemplate <- @(item) Item.Manager.addTemplate(item);

getItemTemplate <- @(instance) Item.Manager.getTemplateByInstance(instance);
getItemTemplateById <- @(itemId) Item.Manager.getTemplateById(itemId);
getItemTemplateIdByInstance <- @(instance) Item.Manager.getIdByInstance(instance);

getItemTemplates <- @() Item.Manager.getTemplates();