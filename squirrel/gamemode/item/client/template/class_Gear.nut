
class Item.Gear extends Item.Standard
{
    _requirement = null;

    constructor(id, instance, visualId, typeId)
    {
            _requirement = {};
        base.constructor(id, instance, visualId, typeId);
    }

    function setRequirement(requirementType, requirement)
    {
        if(requirement <= 0)
            return;
            
        _requirement[requirementType] <- requirement;
    }
    
    function getRequirement(requirementType) { return requirementType in _requirement ? _requirement[requirementType] : 0; }

    function getRequirements() { return _requirement; }
    
    function canBeEquippedByHero()
    {
        local rLevel = getRequirement(ItemRequirement.Level);
        local rMagicLevel = getRequirement(ItemRequirement.MagicLevel);
        local rStrength = getRequirement(ItemRequirement.Strength);
        local rDexterity = getRequirement(ItemRequirement.Dexterity);   
        local rMana = getRequirement(ItemRequirement.Mana);   
        local rHealth = getRequirement(ItemRequirement.Health);   

        if(rLevel > getLevel()) return false;
        if(rMagicLevel > getPlayerMagicLevel(heroId)) return false;
        if(rStrength > getPlayerStrength(heroId)) return false;
        if(rDexterity > getPlayerDexterity(heroId)) return false;
        if(rMana > getPlayerMaxMana(heroId)) return false;
        if(rHealth > getPlayerMaxHealth(heroId)) return false;

        return true;
    }
}