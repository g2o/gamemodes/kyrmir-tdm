
class Item.Armor extends Item.Gear
{
    _protection = null;

    constructor(id, instance, visualId, typeId)
    {
            _protection = {};
        base.constructor(id, instance, visualId, typeId);
    }

    function setProtection(protectionType, protection)
    {
        _protection[protectionType] <- protection;
    }

    function getProtection(protectionType) 
    { 
        return protectionType in _protection ? _protection[protectionType] : 0; 
    }

    function getProtections() { return _protection; }
}