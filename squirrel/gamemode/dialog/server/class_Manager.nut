
class Dialog.Manager
{
    static _npcList = {};

//:protected
    static function _listToPacket()
    {
        local packet = Packet(PacketIDs.Dialog, Dialog_PacketIDs.List);
            packet.writeInt16(_npcList.len()); 

        foreach(index, value in _npcList)
        {
            packet.writeInt16(index);
        }

        return packet;
    }

    static function client_beginConversation(playerId, packet)
    {        
        getPlayer(playerId).getDialogModule().client_beginConversation(packet.readInt16());
    }

    static function client_finishConversation(playerId)
    {        
        getPlayer(playerId).getDialogModule().client_finishConversation();
    }

    static function client_selectOption(playerId, packet)
    {
        getPlayer(playerId).getDialogModule().client_selectOption(packet.readString());    
    }

    static function client_buyItem(playerId, packet)
    {
        getPlayer(playerId).getDialogModule().client_buyItem(packet.readInt16(), packet.readInt32());    
    }

//:public
    static function playerJoin(playerId)
    {
        _listToPacket().send(playerId, RELIABLE);
    }

    static function registerNpc(npcId, initiatorFunc)
    {
        _npcList[npcId] <- initiatorFunc;
    }

    static function getNpcInitiator(playerId, npcId) { return npcId in _npcList ? _npcList[npcId](playerId) : null; }   

    static function packetRead(playerId, packet)
    {
        local packetId = packet.readInt8();

        switch(packetId)
        {
            case Dialog_PacketIDs.BeginConversation: client_beginConversation(playerId, packet); break;
            case Dialog_PacketIDs.FinishConversation: client_finishConversation(playerId); break;
            case Dialog_PacketIDs.SelectOption: client_selectOption(playerId, packet); break;
            case Dialog_PacketIDs.BuyItem: client_buyItem(playerId, packet); break;
        }
    }
}

getDialogManager <- @() Dialog.Manager;

// Dialog
registerDialogNpc <- @(npcId, initiatorFunc) Dialog.Manager.registerNpc(npcId, initiatorFunc);