
class Dialog.Transcription
{
    _instance = null;

    _title = null;
    _dialogList = null;

    constructor(instance)
    {
        _instance = instance;

        _title = null;
        _dialogList = [];
    }

//:public   
    function getInstance() { return _instance; }
    
    function setTitle(translation)
    {
        _title = translation;
            return this;
    }

    function getTitle() { return _title; }
    
    function addDialog(translation, who, timestamp, file = null)
    {
        _dialogList.push({ translation = translation, who = who, timestamp = timestamp, file = file });
            return this;
    }

    function getDialogs() { return _dialogList; }
}