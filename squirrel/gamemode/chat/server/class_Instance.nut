
class Chat.Instance
{
    _prefix = -1;

    _instance = null;
    _color = null;

    _distance = -1;

    _permissionFunc = null;
    _receiverFunc = null;

    _logFile = null;
    
    constructor(prefix, instance)
    {
        _prefix = prefix;

        _instance = instance;
        _color = { r = 255, g = 255, b = 255 };

        _distance = -1;

        _permissionFunc = null;
        _receiverFunc = null;

        _logFile = file("file_db/" + instance + ".log", "a+");
    }

//:public 
    function writeLog(username, text)
    {
            local logTime = date();
        _logFile.write(format("%d/%02d/%02d %02d:%02d - %s: %s\n", logTime.year, logTime.month, logTime.day, logTime.hour, logTime.min, username, text));
    }

    function close()
    {
        _logFile.close();
    }

    function getPrefix() { return _prefix; }

    function getInstance() { return _instance; }

    function setColor(r, g, b)
    {
        _color = { r = r, g = g, b = b };
    }

    function getColor() { return _color; }

    function setDistance(distance)
    {
        _distance = distance;
            return this;
    }

    function getDistance() { return _distance; }

    function bindPermissionFunc(permission)
    {
        _permissionFunc = permission;
            return this;
    }

    function checkPermission(playerId)
    {
        if(_permissionFunc != null)
            return _permissionFunc(playerId);

        return true;
    }

    function bindReceiverFunc(receiver)
    {
        _receiverFunc = receiver;
            return this;
    }

    function checkReceiver(senderId, receiverId)
    {
        if(_receiverFunc != null)
            return _receiverFunc(senderId, receiverId);

        return true;
    }
}
