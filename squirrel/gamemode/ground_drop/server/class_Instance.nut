
class GroundDrop.Instance
{
    _template = null;
    _item = null;

    _disapperanceTime = -1;

    constructor(itemTemplate, amount, x, y, z, world, virtualWorld = 0, disapperanceTime = -1)
    {
        _template = itemTemplate;
        _item = ItemsGround.spawn(itemTemplate.getVisualId(), amount, x, y, z, world, virtualWorld);

        if(disapperanceTime != -1) 
            _disapperanceTime = getTickCount() + disapperanceTime * 1000;
    }
    
//:public
    function getObject() { return _item; } 

    function getTemplate() { return _template; }

    function getAmount() { return _item.amount; }

    function getDisapperanceTime() { return _disapperanceTime; }

    function destroy()
    {
        ItemsGround.destroy(_item.id);
    }
}