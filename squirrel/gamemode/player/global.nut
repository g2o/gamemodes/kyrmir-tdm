
enum RegisterCode 
{
    Succes,
    Fault,
    DataUsed
}

enum LoginCode 
{
    Succes,
    FirstLogin,
    Fault,
    NotExist
}

enum PermissionLevel
{
    None, // 0
    Moderator, // 1
    Administrator, // 2
}