
class Damage.Manager
{    
//:private
    static function _playerToPlayer(killerId, playerId)
    {        
        local context = getPlayerContext(killerId, DAMAGE_CTX),
            hitValue = 0;

        if(context == -1) 
        {
            hitValue = getPlayerStrength(killerId) - getPlayer(playerId).getProtection(getPlayerDamageType(killerId));
        }
        else 
        {
            local template = getItemTemplateById(context);

            switch(template.getType())
            {
                case ItemType.WeaponMelee: 
                {
                    if(random(1, 100) <= getPlayerSkillWeapon(killerId, template.getWeaponMode() == WEAPONMODE_1HS ? WEAPON_1H : WEAPON_2H))
                    {
                        hitValue = (getPlayerStrength(killerId) + template.getDamage()) - getPlayer(playerId).getProtection(template.getDamageType());
                    }
                    else 
                        hitValue = ((getPlayerStrength(killerId) + template.getDamage()) - getPlayer(playerId).getProtection(template.getDamageType())) / 2;
                
                    if(hitValue <= 0) hitValue = 5;
                } 
                break;
                case ItemType.WeaponRanged:
                {                    
                    local hitChance = getPlayerSkillWeapon(killerId, template.getWeaponMode() == WEAPONMODE_BOW ? WEAPON_BOW : WEAPON_CBOW);

                    if(hitChance >= 100)
                    {
                        hitValue = getPlayerDexterity(killerId) + template.getDamage() - getPlayer(playerId).getProtection(template.getDamageType());
                    }
                    else 
                    {
                        local pKiller = getPlayerPosition(killerId), pTarget = getPlayerPosition(playerId);

                        local distance = getDistance3d(pKiller.x, pKiller.y, pKiller.z, pTarget.x, pTarget.y, pTarget.z);
                        
                        if(distance <= RANGED_CHANCE_MINDIST)
                        {
                            hitValue = getPlayerDexterity(killerId) + template.getDamage() - getPlayer(playerId).getProtection(template.getDamageType());
                        }
                        else 
                        {
                            local distanceScale = distance / RANGED_CHANCE_MAXDIST;
                            local hitChance = hitChance + distanceScale * (0.0 - hitChance);

                            if(random(1, 100) <= hitChance)
                            {
                                hitValue = getPlayerDexterity(killerId) + template.getDamage() - getPlayer(playerId).getProtection(template.getDamageType());
                            }
                            else hitValue = 0;
                        }
                    }
                } 
                break;
                case ItemType.Rune:
                case ItemType.Scroll:
                {
                    if(template.callEffectFunc(playerId, killerId)) 
                        return null;
                
                    hitValue = template.getDamage() - getPlayer(playerId).getProtection(template.getDamageType());
                }
                break;
            }            
        }

        return hitValue < 0 ? 0 : hitValue;
    }

    static function _playerToNpc(killerId, npc, context)
    {
        local hitValue = 0;

        if(context == -1) 
        {
            hitValue = getPlayerStrength(killerId) - npc.getProtection(getPlayerDamageType(killerId));
        }
        else 
        {
            local template = getItemTemplateById(context);

            switch(template.getType())
            {
                case ItemType.WeaponMelee: 
                {
                    if(random(1, 100) <= getPlayerSkillWeapon(killerId, template.getWeaponMode() == WEAPONMODE_1HS ? WEAPON_1H : WEAPON_2H))
                    {
                        hitValue = (getPlayerStrength(killerId) + template.getDamage()) - npc.getProtection(template.getDamageType());
                    }
                    else 
                        hitValue = ((getPlayerStrength(killerId) + template.getDamage()) - npc.getProtection(template.getDamageType())) / 2;

                    if(hitValue <= 0) hitValue = 5;
                } 
                break;
                case ItemType.WeaponRanged:
                {                    
                    local hitChance = getPlayerSkillWeapon(killerId, template.getWeaponMode() == WEAPONMODE_BOW ? WEAPON_BOW : WEAPON_CBOW);

                    if(hitChance >= 100)
                    {
                        hitValue = getPlayerDexterity(killerId) + template.getDamage() - npc.getProtection(template.getDamageType());
                    }
                    else 
                    {
                        local pKiller = getPlayerPosition(killerId),
                            pTarget = npc.getPosition();

                        local distance = getDistance3d(pKiller.x, pKiller.y, pKiller.z, pTarget.x, pTarget.y, pTarget.z);
                        
                        if(distance <= RANGED_CHANCE_MINDIST)
                        {
                            hitValue = getPlayerDexterity(killerId) + template.getDamage() - npc.getProtection(template.getDamageType());
                        }
                        else 
                        {
                            local distanceScale = distance / RANGED_CHANCE_MAXDIST;
                            local hitChance = hitChance + distanceScale * (0.0 - hitChance);

                            if(random(1, 100) <= hitChance)
                            {
                                hitValue = getPlayerDexterity(killerId) + template.getDamage() - npc.getProtection(template.getDamageType());
                            }
                            else hitValue = 0;
                        }
                    }
                }
                break;
                case ItemType.Rune:
                case ItemType.Scroll:
                {
                    if(template.callEffectFunc(npc, killerId)) 
                        return null;
                     
                    hitValue = template.getDamage() - npc.getProtection(template.getDamageType());
                }
                break;
            }
        }

        return hitValue < 0 ? 10 : hitValue;
    }

    static function _npcToPlayer(npc, playerId)
    { 
        local hitValue = npc.getDamage() - getPlayer(playerId).getProtection(npc.getDamageType());
            return hitValue <= 0 ? 5 : hitValue;
    }

//:public
    static function client_damageFall(playerId, packet)
    {
        if(isPlayerDead(playerId)) return;

        local newHealth = getPlayerHealth(playerId) - packet.readInt16();
        
        if(newHealth <= 0)
            callEvent("Damage.onPlayerDead", playerId, -1, getPlayerLastHitList(playerId, HIT_SIGNIFICANCE_TIME));

        setPlayerHealth(playerId, newHealth);
    }

    static function client_parade(killerId, packet)
    {   
        getPlayer(packet.readInt16()).getDamageModule().client_parade(killerId);
    }

    static function npcHit(killerId, playerId, context = -1)
    {
        local maxSlots = getMaxSlots();

        if(killerId >= maxSlots)
        {
            if(isPlayerImmortal(playerId)) return;

            local health = getPlayerHealth(playerId);
            if(health <= 0) return;

            local bot = getBot(killerId), 
                newHealth = health - _npcToPlayer(bot, playerId);
            
            if(newHealth <= 0)
                callEvent("Damage.onPlayerDead", playerId, bot.getId(), getPlayerLastHitList(playerId, HIT_SIGNIFICANCE_TIME));

            setPlayerHealth(playerId, newHealth);
        }
        else 
        {
            if(getPlayerInvisible(killerId)) return;

            local bot = getBot(playerId);
            if(!(bot && bot.isAlive())) return;

            if(callEvent("Damage.onPlayerHitNpc", playerId, killerId) == true) 
                return;
            
            getBotManager().hit(killerId, playerId);
            
            local damage = _playerToNpc(killerId, bot, context);

            if(damage != null)
            {
                local newHealth = bot.getHealth() - damage;

                if(newHealth <= 0) 
                    callEvent("Damage.onNpcDead", bot.getId(), killerId, bot.getLastHitList(HIT_SIGNIFICANCE_TIME)); 

                bot.setHealth(newHealth);
            }
            else 
            {
                if(bot.getHealth() <= 0) 
                {
                    callEvent("Damage.onNpcDead", bot.getId(), killerId, bot.getLastHitList(HIT_SIGNIFICANCE_TIME)); 
                }
            }
        }
    }

    static function playerHit(playerId, killerId, desc)
    {
        if(callEvent("Damage.onPlayerHitPlayer", playerId, killerId) == true || isPlayerImmortal(playerId))
        {
            eventValue(0);
            return;
        }
        
        getPlayerManager().playerHit(playerId, killerId);

        local damage = _playerToPlayer(killerId, playerId);

        if(damage != null)
        {
            if(isPlayerDead(playerId) == false)
            {          
                getPlayer(playerId).receiveDamage(damage);  
                    eventValue(damage * -1);

                setPlayerDealtDamage(killerId, getPlayerDealtDamage(killerId) + damage);
            
                if(isPlayerDead(playerId)) callEvent("Damage.onPlayerDead", playerId, killerId, getPlayerLastHitList(playerId, HIT_SIGNIFICANCE_TIME));
            }
        }
    }

    static function playerSpellCast(playerId, item)
    {
        local targetId = getPlayerFocus(playerId);

        if(targetId == -1)
            item.callEffectFunc(-1, playerId);
        else 
        {
            if(isPlayerDead(targetId) || item.isInstant() == false) 
                return;

            getPlayerManager().playerHit(targetId, playerId);

            item.callEffectFunc(targetId, playerId);

            if(isPlayerDead(targetId)) 
                callEvent("Damage.onPlayerDead", targetId, playerId, getPlayerLastHitList(playerId, HIT_SIGNIFICANCE_TIME));
        }
    }

    static function playerDead(playerId, killerId)
    {
        if(getPlayerKillerId(playerId) == -1) callEvent("Damage.onPlayerDead", playerId, killerId, getPlayerLastHitList(playerId, HIT_SIGNIFICANCE_TIME));
    }

    static function checkParade()
    {
        local playerList = getPlayers(), 
            tick = getTickCount();

        foreach(player in playerList)
        {
            player.getDamageModule().regenerate(tick);
        }
    }
    
    static function packetRead(playerId, packet)
    {
        local packetId = packet.readInt8();

        switch(packetId)
        {
            case Damage_PacketIDs.Parade: client_parade(playerId, packet); break;
            case Damage_PacketIDs.Damage_Fall: client_damageFall(playerId, packet); break;
        } 
    }
}

getDamageManager <- @() Damage.Manager;