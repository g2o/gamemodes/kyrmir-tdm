
// Register packet callback
PacketReceive.add(PacketIDs.Damage, getDamageManager());

setTimer(Damage.Manager.checkParade.bindenv(Damage.Manager), 100, 0);

addEventHandler("Equipment.onPlayerSpellCast", Damage.Manager.playerSpellCast.bindenv(Damage.Manager));

addEventHandler("Npc.onHit", Damage.Manager.npcHit.bindenv(Damage.Manager));

addEventHandler("onPlayerHit", Damage.Manager.playerHit.bindenv(Damage.Manager));

addEventHandler("onPlayerDead", Damage.Manager.playerDead.bindenv(Damage.Manager));

