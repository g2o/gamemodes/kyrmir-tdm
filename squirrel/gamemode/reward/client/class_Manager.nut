
class Reward.Manager 
{    
    static function server_monsterStreak(packet)
    {
        getHero().getRewardModule().setMonsterStreak(packet.readInt32());
    }

    static function server_playerStreak(packet)
    {
        getHero().getRewardModule().setPlayerStreak(packet.readInt32());
    }

//:public
    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Reward_PacketIDs.MonsterStreak: server_monsterStreak(packet); break;	
            case Reward_PacketIDs.PlayerStreak: server_playerStreak(packet); break;	
        }
    }
}

getRewardManager <- @() Reward.Manager;

// Reward
getHeroMonsterStreak <- @() getHero().getRewardModule().getMonsterStreak();
getHeroPlayerStreak <- @() getHero().getRewardModule().getPlayerStreak();