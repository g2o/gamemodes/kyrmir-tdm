
class Guild.Instance 
{
    _id = null;

    _color = null;

    _homeWorld = null;
    _respawnPosition = null;

    _pointCounter = 0;
    _memberCounter = 0;
    _locked = false;

    _armors = null;

    constructor(id, homeWorld, color_r = 255, color_g = 255, color_b = 255)
    {
        _id = id;

        _color = { r = color_r, g = color_g, b = color_b };
        
        _homeWorld = homeWorld;
        _respawnPosition = { x = 0, y = 100, z = 100 };

        _pointCounter = 0;
        _memberCounter = 0;  
        _locked = false;

        _armors = {};
    }

//:public 
    function getId() { return _id; }

    function getColor() { return _color; }

    function getHomeWorld() { return _homeWorld; }
    
    function setRespawnPosition(x, y, z)
    {
        _respawnPosition.x = x;
        _respawnPosition.y = y;
        _respawnPosition.z = z;

        return this;
    }

    function getRespawnPosition() { return _respawnPosition; }

    function setPoints(points)
    {
        if(_pointCounter != points)
        {
            local packet = Packet(PacketIDs.Guild, Guild_PacketIDs.Points);

                packet.writeString(_id);
                packet.writeInt32(points);
            
            callEvent("Guild.onPointsUpdate", _id, _pointCounter = points);

            packet.sendToAll(UNRELIABLE);  
        }
    }

    function getPoints() { return _pointCounter; }

    function setMembers(members)
    {
        if(_memberCounter != members)
        {
            local packet = Packet(PacketIDs.Guild, Guild_PacketIDs.Members);

                packet.writeString(_id);
                packet.writeInt8(members);

            callEvent("Guild.onMembersUpdate", _id, _memberCounter = members);

            packet.sendToAll(UNRELIABLE);  
        }
    }

    function getMembers() { return _memberCounter; }

    function setLock(locked)
    {
        if(_locked != locked)
        {
            local packet = Packet(PacketIDs.Guild, Guild_PacketIDs.Lock);

                packet.writeString(_id);
                packet.writeBool(_locked = locked);

            packet.sendToAll(RELIABLE);  
        }
    }

    function isLocked() { return _locked; }

    function registerArmor(level, instance)
    {
        _armors[level] <- instance;
            return this;
    }

    function checkItem(instance)
    {
        foreach(level, armorInstance in _armors)
        {
            if(armorInstance == instance)
            {
                return level;
            }
        }

        return -1;
    }

    function getArmor(armorLevel)
    {
        return armorLevel in _armors ? _armors[armorLevel] : null;
    }

    function getArmors() { return _armors; }

    function reset()
    {
        _pointCounter = 0;
        _memberCounter = 0;

        local packet = Packet(PacketIDs.Guild, Guild_PacketIDs.Reset);
            packet.writeString(_id);
        packet.sendToAll(RELIABLE);

        callEvent("Guild.onReset", _id);
    }
}