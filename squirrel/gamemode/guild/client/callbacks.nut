
// Register packet callback
PacketReceive.add(PacketIDs.Guild, getGuildManager());

// Barrier - domination marker
local DISABLE_BARRIER = true;

local r = 255, g = 255, b = 255;
local target_r = 255, target_g = 255, target_b = 255;

local timer = -1;

local timerFunc = function()
{
    local change = false;

    if(target_r != r)
    {
        local increment = target_r > r ? 1 : -1;
            r = r + increment;

        change = true;
    }
    if(target_g != g)
    {
        local increment = target_g > g ? 1 : -1;
            g = g + increment;

        change = true;
    }
    if(target_b != b)
    {
        local increment = target_b > b ? 1 : -1;
            b = b + increment;

        change = true;
    }

    if(change) 
        _setBarrierColor(r, g, b);
    else 
    {
        killTimer(timer);
            timer = -1;
    }
};

addEventHandler("onInit", function()
{
    if("setBarrierOpacity" in getroottable())
    {
        setBarrierOpacity(0);

        if("setBarrierColor" in getroottable())
        {
            _setBarrierColor <- setBarrierColor;

            setBarrierColor <- function(r, g, b)
            {
                target_r = r;
                target_g = g;
                target_b = b;

                if(timer == -1)
                {
                    timer = setTimer(timerFunc, 50, 0);
                }
            }
        }
    }
});

local function compare(item_a, item_b) 
{
    local type_a = item_a.getPoints(),
        type_b = item_b.getPoints();

    if(type_a < type_b) return -1;
    if(type_a > type_b) return 1;

    return 0;
}

local function updateColor()
{
    local guilds = getGuilds(), newArray = [];

    foreach(guild in guilds) newArray.push(guild);
    
    newArray.sort(compare);

    local arraySize = newArray.len(), leader = arraySize - 1;

    if(newArray[leader].getPoints() != newArray[arraySize - 2].getPoints())
    {
            local color = newArray[leader].getColor();
        setBarrierColor(color.r, color.g, color.b);
    }
    else
        setBarrierColor(255, 255, 255);
}

addEventHandler("Guild.onInitialData", function()
{
    if("setBarrierOpacity" in getroottable())
    {
        if(DISABLE_BARRIER)
        {
            DISABLE_BARRIER = false;
                setBarrierOpacity(255);

            updateColor();
        }
    }
});

addEventHandler("Guild.onPointsUpdate", function(guildId, points)
{
    if(DISABLE_BARRIER == false)
    {
        updateColor();
    }
});