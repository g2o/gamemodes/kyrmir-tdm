
class Area.Player
{
    _playerId = -1;
    _areaList = null;

    constructor(playerId)
    {
        _playerId = playerId;
            _areaList = [];
    }

//:public
    function enterArea(area)
    {
        if(_areaList == null) _areaList = [];

        _areaList.push(area);
        
        local packet = Packet(PacketIDs.Area, Area_PacketIDs.Enter)
            packet.writeString(area.getInstance());    
        packet.send(_playerId, RELIABLE_ORDERED);
    }

    function leaveArea(area)
    {
        local id = _areaList.find(area);
        
        if(id != null)
        {
            _areaList.remove(id);

            if(_areaList.len() == 0)
                _areaList = null;

            local packet = Packet(PacketIDs.Area, Area_PacketIDs.Leave)
                packet.writeString(area.getInstance());    
            packet.send(_playerId, RELIABLE_ORDERED);               
        }
    }
        
    function playerDead()
    {
        if(_areaList != null)
        {
            Packet(PacketIDs.Area, Area_PacketIDs.Clear).send(_playerId, RELIABLE_ORDERED);              
        }

        _areaList = null;
    }

    function isInArea(area)
    {
        if(_areaList != null)
        {
            foreach(listArea in _areaList)
            {
                if(area == listArea)
                    return true;
            }
        }
        
        return false;
    }
}