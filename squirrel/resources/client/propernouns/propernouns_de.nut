
/********************* MONSTERS *********************/

registerTranslation("KYRMIR_BLOODFLY", LANG.DE, "Blutfliege");
registerTranslation("KYRMIR_BLOODHOUND", LANG.DE, "Bluthund");
registerTranslation("KYRMIR_GOBBO_BLACK", LANG.DE, "Schwarzer Goblin");
registerTranslation("KYRMIR_GOBBO_GREEN", LANG.DE, "Goblin");
registerTranslation("KYRMIR_GOBBO_WARRIOR", LANG.DE, "Goblin Krieger");
registerTranslation("KYRMIR_HARPIE", LANG.DE, "Harpie");
registerTranslation("KYRMIR_LURKER", LANG.DE, "Lurker");
registerTranslation("KYRMIR_MINECRAWLER", LANG.DE, "Minecrawler");
registerTranslation("KYRMIR_MINECRAWLERWARRIOR", LANG.DE, "Minecrawler Warrior");
registerTranslation("KYRMIR_MOLERAT", LANG.DE, "Molerat");
registerTranslation("KYRMIR_ORCBITER", LANG.DE, "Beisser");
registerTranslation("KYRMIR_ORCSHAMAN_SIT", LANG.DE, "Ork Schamane");
registerTranslation("KYRMIR_ORCWARRIOR_ROAM", LANG.DE, "Ork Krieger");
registerTranslation("KYRMIR_RAZOR", LANG.DE, "Razor");
registerTranslation("KYRMIR_SCAVENGER", LANG.DE, "Scavenger");
registerTranslation("KYRMIR_SHADOWBEAST", LANG.DE, "Schattenläufer");
registerTranslation("KYRMIR_SKELETON", LANG.DE, "Skelett Krieger");
registerTranslation("KYRMIR_SNAPPER", LANG.DE, "Snapper");
registerTranslation("KYRMIR_SWAMPSHARK", LANG.DE, "Sumpfhai");
registerTranslation("KYRMIR_UNDEADORCWARRIOR", LANG.DE, "Untoter Ork");
registerTranslation("KYRMIR_WARAN", LANG.DE, "Waran");
registerTranslation("KYRMIR_WARG", LANG.DE, "Warg");
registerTranslation("KYRMIR_WOLF", LANG.DE, "Wolf");
registerTranslation("KYRMIR_BLATTCRAWLER", LANG.DE, "Fangheuschrecke");
registerTranslation("KYRMIR_DRAGONSNAPPER", LANG.DE, "Drachensnapper");
registerTranslation("KYRMIR_GIANT_DESERTRAT", LANG.DE, "Wüstenratte");
registerTranslation("KYRMIR_GOBBO_SKELETON", LANG.DE, "Goblin Skelett");
registerTranslation("KYRMIR_GOBBO_WARRIOR_VISIR", LANG.DE, "Goblinkrieger");
registerTranslation("KYRMIR_KEILER", LANG.DE, "Keiler");
registerTranslation("KYRMIR_MAYAZOMBIE03", LANG.DE, "Uralter Zombie");
registerTranslation("KYRMIR_ORCELITE_ROAM", LANG.DE, "Ork Elite");
registerTranslation("KYRMIR_SCAVENGER_DEMON", LANG.DE, "Graslandscavenger");
registerTranslation("KYRMIR_SHADOWBEAST_ADDON_FIRE", LANG.DE, "Feuerteufel");
registerTranslation("KYRMIR_STONEGUARDIAN", LANG.DE, "Steinwächter");
registerTranslation("KYRMIR_SWAMPDRONE", LANG.DE, "Sumpfgasdrohne");
registerTranslation("KYRMIR_SWAMPGOLEM", LANG.DE, "Sumpfgolem");
registerTranslation("KYRMIR_SWAMPRAT", LANG.DE, "Sumpfratte");
registerTranslation("KYRMIR_TROLL", LANG.DE, "Troll");
registerTranslation("KYRMIR_ZOMBIE_ADDON_KNECHT", LANG.DE, "Zombie Knecht");
registerTranslation("KYRMIR_BOSS_BLACK", LANG.DE, "Schwarzer Troll");
registerTranslation("KYRMIR_BOSS_GREEN", LANG.DE, "Grüner Skelett-Paladin");
registerTranslation("KYRMIR_BOSS_WHITE", LANG.DE, "Weißer Schattenläufer");
registerTranslation("KYRMIR_BOSS_RED", LANG.DE, "Roter Zombie");
registerTranslation("KYRMIR_BOSS_BLUE", LANG.DE, "Blauer Golem");
registerTranslation("KYRMIR_BOSS_VIOLET", LANG.DE, "Violetter Skelettmagier");
registerTranslation("KYRMIR_BOSS_YELLOW", LANG.DE, "Gelber Dämonenlord");
registerTranslation("KYRMIR_ALEXIA", LANG.DE, "Alexia");
registerTranslation("KYRMIR_MINIBOSS_GREEN", LANG.DE, "Grüner Lurker");
registerTranslation("KYRMIR_MINIBOSS_RED", LANG.DE, "Roter Warg");
registerTranslation("KYRMIR_MINIBOSS_YELLOW", LANG.DE, "Gelber Snapper");
registerTranslation("KYRMIR_MINIBOSS_GRAY", LANG.DE, "Grauer Razor");
registerTranslation("KYRMIR_MINIBOSS_VIOLET", LANG.DE, "Violetter Minecrawler Warrior");
registerTranslation("KYRMIR_MINIBOSS_BLUE", LANG.DE, "Blauer Scavenger");

/********************* NPCS *********************/

registerTranslation("NPC_ARMOR", LANG.DE, "Rüstungshändler");
registerTranslation("NPC_BOW", LANG.DE, "Bogenhändler");
registerTranslation("NPC_CROSSBOW", LANG.DE, "Armbrusthändler");
registerTranslation("NPC_POTION", LANG.DE, "Trankhändler");
registerTranslation("NPC_RUNE", LANG.DE, "Runenhändler");
registerTranslation("NPC_SKILL", LANG.DE, "Lehrer");
registerTranslation("NPC_WEAPON_1H", LANG.DE, "Einhandwaffenhändler");
registerTranslation("NPC_WEAPON_2H", LANG.DE, "Zweihandwaffenhändler");
registerTranslation("NPC_ARENA", LANG.DE, "Arena-Meister");
