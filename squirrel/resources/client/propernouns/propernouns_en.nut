
/********************* MONSTERS *********************/

registerTranslation("KYRMIR_BLOODFLY", LANG.EN, "Bloodfly");
registerTranslation("KYRMIR_BLOODHOUND", LANG.EN, "Bloodhound");
registerTranslation("KYRMIR_GOBBO_BLACK", LANG.EN, "Black Goblin");
registerTranslation("KYRMIR_GOBBO_GREEN", LANG.EN, "Goblin");
registerTranslation("KYRMIR_GOBBO_WARRIOR", LANG.EN, "Goblin Warrior");
registerTranslation("KYRMIR_HARPIE", LANG.EN, "Harpy");
registerTranslation("KYRMIR_LURKER", LANG.EN, "Lurker");
registerTranslation("KYRMIR_MINECRAWLER", LANG.EN, "Minecrawler");
registerTranslation("KYRMIR_MINECRAWLERWARRIOR", LANG.EN, "Minecrawler Warrior");
registerTranslation("KYRMIR_MOLERAT", LANG.EN, "Molerat");
registerTranslation("KYRMIR_ORCBITER", LANG.EN, "Biter");
registerTranslation("KYRMIR_ORCSHAMAN_SIT", LANG.EN, "Orc Shaman");
registerTranslation("KYRMIR_ORCWARRIOR_ROAM", LANG.EN, "Orc Warrior");
registerTranslation("KYRMIR_RAZOR", LANG.EN, "Razor");
registerTranslation("KYRMIR_SCAVENGER", LANG.EN, "Scavenger");
registerTranslation("KYRMIR_SHADOWBEAST", LANG.EN, "Shadowbeast");
registerTranslation("KYRMIR_SKELETON", LANG.EN, "Skeleton Warrior");
registerTranslation("KYRMIR_SNAPPER", LANG.EN, "Snapper");
registerTranslation("KYRMIR_SWAMPSHARK", LANG.EN, "Swampshark");
registerTranslation("KYRMIR_UNDEADORCWARRIOR", LANG.EN, "Undead Orc");
registerTranslation("KYRMIR_WARAN", LANG.EN, "Lizard");
registerTranslation("KYRMIR_WARG", LANG.EN, "Warg");
registerTranslation("KYRMIR_WOLF", LANG.EN, "Wolf");
registerTranslation("KYRMIR_BLATTCRAWLER", LANG.EN, "Mantis");
registerTranslation("KYRMIR_DRAGONSNAPPER", LANG.EN, "Dragon Snapper");
registerTranslation("KYRMIR_GIANT_DESERTRAT", LANG.EN, "Desert Rat");
registerTranslation("KYRMIR_GOBBO_SKELETON", LANG.EN, "Goblin Skeleton");
registerTranslation("KYRMIR_GOBBO_WARRIOR_VISIR", LANG.EN, "Goblin Warrior");
registerTranslation("KYRMIR_KEILER", LANG.EN, "Boar");
registerTranslation("KYRMIR_MAYAZOMBIE03", LANG.EN, "Ancient Zombie");
registerTranslation("KYRMIR_ORCELITE_ROAM", LANG.EN, "Orc Elite");
registerTranslation("KYRMIR_SCAVENGER_DEMON", LANG.EN, "Grassland Scavenger");
registerTranslation("KYRMIR_SHADOWBEAST_ADDON_FIRE", LANG.EN, "Fire Devil");
registerTranslation("KYRMIR_STONEGUARDIAN", LANG.EN, "Stone Guardian");
registerTranslation("KYRMIR_SWAMPDRONE", LANG.EN, "Swampgas Drone");
registerTranslation("KYRMIR_SWAMPGOLEM", LANG.EN, "Swamp Golem");
registerTranslation("KYRMIR_SWAMPRAT", LANG.EN, "Swamp Rat");
registerTranslation("KYRMIR_TROLL", LANG.EN, "Troll");
registerTranslation("KYRMIR_ZOMBIE_ADDON_KNECHT", LANG.EN, "Zombie Warden");
registerTranslation("KYRMIR_BOSS_BLACK", LANG.EN, "Black Troll");
registerTranslation("KYRMIR_BOSS_GREEN", LANG.EN, "Green Skeleton Paladin");
registerTranslation("KYRMIR_BOSS_WHITE", LANG.EN, "White Shadowbeast");
registerTranslation("KYRMIR_BOSS_RED", LANG.EN, "Red Zombie");
registerTranslation("KYRMIR_BOSS_BLUE", LANG.EN, "Blue Golem");
registerTranslation("KYRMIR_BOSS_VIOLET", LANG.EN, "Violet Skeleton Mage");
registerTranslation("KYRMIR_BOSS_YELLOW", LANG.EN, "Yellow Demon Lord");
registerTranslation("KYRMIR_ALEXIA", LANG.EN, "Alexia");
registerTranslation("KYRMIR_MINIBOSS_GREEN", LANG.EN, "Green Lurker");
registerTranslation("KYRMIR_MINIBOSS_RED", LANG.EN, "Red Warg");
registerTranslation("KYRMIR_MINIBOSS_YELLOW", LANG.EN, "Yellow Snapper");
registerTranslation("KYRMIR_MINIBOSS_GRAY", LANG.EN, "Gray Razor");
registerTranslation("KYRMIR_MINIBOSS_VIOLET", LANG.EN, "Violet Minecrawler Warrior");
registerTranslation("KYRMIR_MINIBOSS_BLUE", LANG.EN, "Blue Scavenger");

/********************* NPCS *********************/

registerTranslation("NPC_ARMOR", LANG.EN, "Armor Merchant");
registerTranslation("NPC_BOW", LANG.EN, "Bow Merchant");
registerTranslation("NPC_CROSSBOW", LANG.EN, "Crossbow Merchant");
registerTranslation("NPC_POTION", LANG.EN, "Potion Merchant");
registerTranslation("NPC_RUNE", LANG.EN, "Rune Merchant");
registerTranslation("NPC_SKILL", LANG.EN, "Skill Teacher");
registerTranslation("NPC_WEAPON_1H", LANG.EN, "One-Handed Weapons Merchant");
registerTranslation("NPC_WEAPON_2H", LANG.EN, "Two-Handed Weapons Merchant");
registerTranslation("NPC_ARENA", LANG.EN, "Arena Master");
