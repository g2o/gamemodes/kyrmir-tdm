
/********************* ITAR *********************/

registerTranslation("ITAR_SO_01" LANG.EN, "Convict's Rags");
registerTranslation("ITAR_SO_02" LANG.EN, "Light Shadow's Dress");
registerTranslation("ITAR_SO_03" LANG.EN, "Medium Shadow's Dress");
registerTranslation("ITAR_SO_04" LANG.EN, "Light Guard's Armor");
registerTranslation("ITAR_SO_05" LANG.EN, "Medium Guard's Armor");
registerTranslation("ITAR_SO_06" LANG.EN, "Heavy Guard's Armor");
registerTranslation("ITAR_SO_07" LANG.EN, "Fire Novice's Robe");
registerTranslation("ITAR_SO_08" LANG.EN, "Fire Mage's Robe");
registerTranslation("ITAR_SO_09" LANG.EN, "Final Robe");
registerTranslation("ITAR_SO_10" LANG.EN, "Final Armor");
registerTranslation("ITAR_NO_01" LANG.EN, "Rogue's Trousers");
registerTranslation("ITAR_NO_02" LANG.EN, "Light Rogue's Dress");
registerTranslation("ITAR_NO_03" LANG.EN, "Medium Rogue's Dress");
registerTranslation("ITAR_NO_04" LANG.EN, "Light Mercenary's Armor");
registerTranslation("ITAR_NO_05" LANG.EN, "Medium Mercenary's Armor");
registerTranslation("ITAR_NO_06" LANG.EN, "Heavy Mercenary's Armor");
registerTranslation("ITAR_NO_07" LANG.EN, "Water Novice's Robe");
registerTranslation("ITAR_NO_08" LANG.EN, "Water Mage's Robe");
registerTranslation("ITAR_NO_09" LANG.EN, "Final Robe");
registerTranslation("ITAR_NO_10" LANG.EN, "Final Armor");
registerTranslation("ITAR_ONB_01" LANG.EN, "Novice's Loincloth");
registerTranslation("ITAR_ONB_02" LANG.EN, "Light Novice's Armor");
registerTranslation("ITAR_ONB_03" LANG.EN, "Medium Novice's Armor");
registerTranslation("ITAR_ONB_04" LANG.EN, "Light Templar's Armor");
registerTranslation("ITAR_ONB_05" LANG.EN, "Medium Templar's Armor");
registerTranslation("ITAR_ONB_06" LANG.EN, "Heavy Templar's Armor");
registerTranslation("ITAR_ONB_07" LANG.EN, "Guru's Robe");
registerTranslation("ITAR_ONB_08" LANG.EN, "High Guru's Robe");
registerTranslation("ITAR_ONB_09" LANG.EN, "Final Robe");
registerTranslation("ITAR_ONB_10" LANG.EN, "Final Armor");
registerTranslation("ITAR_PIR_01" LANG.EN, "Seafarer's Pants");
registerTranslation("ITAR_PIR_02" LANG.EN, "Sailor's Clothing");
registerTranslation("ITAR_PIR_03" LANG.EN, "Light Pirate Armor");
registerTranslation("ITAR_PIR_04" LANG.EN, "Medium Pirate Armor");
registerTranslation("ITAR_PIR_05" LANG.EN, "Heavy Pirate Armor");
registerTranslation("ITAR_PIR_06" LANG.EN, "Storm Sailor's Armor");
registerTranslation("ITAR_PIR_07" LANG.EN, "Water Novice's Robe");
registerTranslation("ITAR_PIR_08" LANG.EN, "Water Mage's Robe");
registerTranslation("ITAR_PIR_09" LANG.EN, "Final Robe");
registerTranslation("ITAR_PIR_10" LANG.EN, "Final Armor");
registerTranslation("ITAR_BDT_01" LANG.EN, "Convict's Rags");
registerTranslation("ITAR_BDT_02" LANG.EN, "Leather Armor");
registerTranslation("ITAR_BDT_03" LANG.EN, "Light Bandit Armor");
registerTranslation("ITAR_BDT_04" LANG.EN, "Medium Bandit Armor");
registerTranslation("ITAR_BDT_05" LANG.EN, "Bandit Guard Armor");
registerTranslation("ITAR_BDT_06" LANG.EN, "Elite Bandit Armor");
registerTranslation("ITAR_BDT_07" LANG.EN, "Novice's Clothing");
registerTranslation("ITAR_BDT_08" LANG.EN, "Necromancer's Robe");
registerTranslation("ITAR_BDT_09" LANG.EN, "Final Robe");
registerTranslation("ITAR_BDT_10" LANG.EN, "Final Armor");

/********************* ITMW *********************/

registerTranslation("ITMW_1H_01" LANG.EN, "Branch");
registerTranslation("ITMW_1H_02" LANG.EN, "Dagger");
registerTranslation("ITMW_1H_03" LANG.EN, "Rough Short Sword");
registerTranslation("ITMW_1H_04" LANG.EN, "Heavy Spiked Cudgel");
registerTranslation("ITMW_1H_05" LANG.EN, "Rough Sword");
registerTranslation("ITMW_1H_06" LANG.EN, "Ship Axe");
registerTranslation("ITMW_1H_07" LANG.EN, "Paladin's Sword");
registerTranslation("ITMW_1H_08" LANG.EN, "Longsword");
registerTranslation("ITMW_1H_09" LANG.EN, "Fine Longsword");
registerTranslation("ITMW_1H_10" LANG.EN, "Fine Bastard Sword");
registerTranslation("ITMW_1H_11" LANG.EN, "Ore Dragon Slayer");
registerTranslation("ITMW_1H_12" LANG.EN, "Claw of Beliar");
registerTranslation("ITMW_2H_01" LANG.EN, "Rusty Two Hander");
registerTranslation("ITMW_2H_02" LANG.EN, "Judge's Staff");
registerTranslation("ITMW_2H_03" LANG.EN, "Light Two Hander");
registerTranslation("ITMW_2H_04" LANG.EN, "Paladin's Two Hander");
registerTranslation("ITMW_2H_05" LANG.EN, "Rune Power");
registerTranslation("ITMW_2H_06" LANG.EN, "Heavy Two Hander");
registerTranslation("ITMW_2H_07" LANG.EN, "Dragon Slicer");
registerTranslation("ITMW_2H_08" LANG.EN, "Holy Executioner");
registerTranslation("ITMW_2H_09" LANG.EN, "Stormbringer");
registerTranslation("ITMW_2H_10" LANG.EN, "Large Ore Dragon Slayer");
registerTranslation("ITMW_2H_11" LANG.EN, "Orcish War Sword");

/********************* ITRW *********************/

registerTranslation("ITRW_BOW_ARROW" LANG.EN, "Arrow");
registerTranslation("ITRW_CROSSBOW_BOLT" LANG.EN, "Bolt");
registerTranslation("ITRW_MAGICBOW_ARROW" LANG.EN, "Magic Arrow");
registerTranslation("ITRW_MAGICCROSSBOW_BOLT" LANG.EN, "Magic Bolt");
registerTranslation("ITRW_BOW_01" LANG.EN, "Shortbow");
registerTranslation("ITRW_BOW_02" LANG.EN, "Willow Bow");
registerTranslation("ITRW_BOW_03" LANG.EN, "Hunting Bow");
registerTranslation("ITRW_BOW_04" LANG.EN, "Elm Bow");
registerTranslation("ITRW_BOW_05" LANG.EN, "Composite Bow");
registerTranslation("ITRW_BOW_06" LANG.EN, "Ash Bow");
registerTranslation("ITRW_BOW_07" LANG.EN, "Longbow");
registerTranslation("ITRW_BOW_08" LANG.EN, "Beech Bow");
registerTranslation("ITRW_BOW_09" LANG.EN, "Bone Bow");
registerTranslation("ITRW_BOW_10" LANG.EN, "Oak Bow");
registerTranslation("ITRW_BOW_11" LANG.EN, "Magic Bow");
registerTranslation("ITRW_CROSSBOW_01" LANG.EN, "Old Crossbow");
registerTranslation("ITRW_CROSSBOW_02" LANG.EN, "Light Crossbow");
registerTranslation("ITRW_CROSSBOW_03" LANG.EN, "Good Crossbow");
registerTranslation("ITRW_CROSSBOW_04" LANG.EN, "Hunting Crossbow");
registerTranslation("ITRW_CROSSBOW_05" LANG.EN, "Battle Crossbow");
registerTranslation("ITRW_CROSSBOW_06" LANG.EN, "Reinforced Crossbow");
registerTranslation("ITRW_CROSSBOW_07" LANG.EN, "Elite Crossbow");
registerTranslation("ITRW_CROSSBOW_08" LANG.EN, "Heavy Crossbow");
registerTranslation("ITRW_CROSSBOW_09" LANG.EN, "Nordmarian Crossbow");
registerTranslation("ITRW_CROSSBOW_10" LANG.EN, "Dragon Hunter's Crossbow");
registerTranslation("ITRW_CROSSBOW_11" LANG.EN, "Magic Crossbow");

/********************* ITRU *********************/

registerTranslation("ITRU_FIREBOLT" LANG.EN, "Fire Arrow");
registerTranslation("ITRU_ICEBOLT" LANG.EN, "Ice Arrow");
registerTranslation("ITRU_LIGHTHEAL" LANG.EN, "Heal Light Wounds");
registerTranslation("ITRU_ZAP" LANG.EN, "Small Lightning");
registerTranslation("ITRU_MEDIUMHEAL" LANG.EN, "Heal Medium Wounds");
registerTranslation("ITRU_ICELANCE" LANG.EN, "Ice Lance");
registerTranslation("ITRU_HEAL_TARGET" LANG.EN, "Target Heal");
registerTranslation("ITRU_HEAL_TARGET_DESC", LANG.EN, "Heal another player (100 health points)");
registerTranslation("ITRU_FIRESTORM" LANG.EN, "Small Fire Storm");
registerTranslation("ITRU_FULLHEAL" LANG.EN, "Heal Heavy Wounds");
registerTranslation("ITRU_INSTANTFIREBALL" LANG.EN, "Fireball");
registerTranslation("ITRU_CONCUSSIONBOLT" LANG.EN, "Holy Arrow");

/********************* ITSC *********************/

registerTranslation("ITSC_MASSDEATH", LANG.EN, "Wave of Death");

/********************* ITPO *********************/

registerTranslation("ITPO_HEALTH_00" LANG.EN, "Novice's Potion");
registerTranslation("ITPO_HEALTH_01" LANG.EN, "Minor Healing Potion");
registerTranslation("ITPO_HEALTH_02" LANG.EN, "Medium Healing Potion");
registerTranslation("ITPO_HEALTH_03" LANG.EN, "Large Healing Potion");
registerTranslation("ITPO_HEALTH_04" LANG.EN, "Large Healing Potion");
registerTranslation("ITPO_MANA_01" LANG.EN, "Minor Mana Potion");
registerTranslation("ITPO_MANA_02" LANG.EN, "Medium Mana Potion");
registerTranslation("ITPO_MANA_03" LANG.EN, "Large Mana Potion");
registerTranslation("ITPO_SPEED" LANG.EN, "Speed Potion");
registerTranslation("ITPO_SPEED_EFFECT" LANG.EN, "Sprint for 30 seconds");

/********************* ITMI *********************/

registerTranslation("ITMI_GOLD" LANG.EN, "Gold");
registerTranslation("ITMI_GOLD_DESC" LANG.EN, "It`s just a gold coin...");
registerTranslation("ITMI_FOCUS_RED" LANG.EN, "Red Focus Stone");
registerTranslation("ITMI_FOCUS_RED_DESC" LANG.EN, "Bonus: 50 Health Points - Drops of the equipment when you die");
registerTranslation("ITMI_FOCUS_YELLOW" LANG.EN, "Yellow Focus Stone");
registerTranslation("ITMI_FOCUS_YELLOW_DESC" LANG.EN, "Bonus: 10 Strength - Drops of the equipment when you die");
registerTranslation("ITMI_FOCUS_GRAY" LANG.EN, "Gray Focus Scroll");
registerTranslation("ITMI_FOCUS_GRAY_DESC" LANG.EN, "Transform into a Razor - To revert use Z - Drops of the equipment when you die");
registerTranslation("ITMI_FOCUS_VIOLET" LANG.EN, "Violet Focus Scroll");
registerTranslation("ITMI_FOCUS_VIOLET_DESC" LANG.EN, "Transform into a Minecrawler Warrior - To revert use Z - Drops of the equipment when you die");
registerTranslation("ITMI_FOCUS_BLUE" LANG.EN, "Blue Focus Stone");
registerTranslation("ITMI_FOCUS_BLUE_DESC" LANG.EN, "Bonus: 20 Mana - Drops of the equipment when you die");
registerTranslation("ITMI_FOCUS_GREEN" LANG.EN, "Green Focus Stone");
registerTranslation("ITMI_FOCUS_GREEN_DESC" LANG.EN, "Bonus: 10 Dexterity - Drops of the equipment when you die");
