
/********************* ITAR *********************/

registerTranslation("ITAR_SO_01", LANG.DE, "Buddlerhose");
registerTranslation("ITAR_SO_02", LANG.DE, "Schattenkluft");
registerTranslation("ITAR_SO_03", LANG.DE, "Schattenr�stung");
registerTranslation("ITAR_SO_04", LANG.DE, "Leichte Garder�stung");
registerTranslation("ITAR_SO_05", LANG.DE, "Mittlere Garder�stung");
registerTranslation("ITAR_SO_06", LANG.DE, "Schwere Garder�stung");
registerTranslation("ITAR_SO_07", LANG.DE, "Feuernovizen Robe");
registerTranslation("ITAR_SO_08", LANG.DE, "Feuermagier Robe");
registerTranslation("ITAR_SO_09", LANG.DE, "Boss Robe");
registerTranslation("ITAR_SO_10", LANG.DE, "Boss R�stung");
registerTranslation("ITAR_NO_01", LANG.DE, "Sch�rferhose");
registerTranslation("ITAR_NO_02", LANG.DE, "Leichte Banditenr�stung");
registerTranslation("ITAR_NO_03", LANG.DE, "Mittlere Banditenr�stung");
registerTranslation("ITAR_NO_04", LANG.DE, "Leichte S�ldnerr�stung");
registerTranslation("ITAR_NO_05", LANG.DE, "Mittlere S�ldnerr�stung");
registerTranslation("ITAR_NO_06", LANG.DE, "Schwere S�ldnerr�stung");
registerTranslation("ITAR_NO_07", LANG.DE, "Wassernovizen Robe");
registerTranslation("ITAR_NO_08", LANG.DE, "Wassermagier Robe");
registerTranslation("ITAR_NO_09", LANG.DE, "Boss Robe");
registerTranslation("ITAR_NO_10", LANG.DE, "Boss R�stung");
registerTranslation("ITAR_ONB_01", LANG.DE, "Novizenrock");
registerTranslation("ITAR_ONB_02", LANG.DE, "Leichte Novizenr�stung");
registerTranslation("ITAR_ONB_03", LANG.DE, "Mittlere Novizenr�stung");
registerTranslation("ITAR_ONB_04", LANG.DE, "Leichte Templerr�stung");
registerTranslation("ITAR_ONB_05", LANG.DE, "Mittlere Templerr�stung");
registerTranslation("ITAR_ONB_06", LANG.DE, "Schwere Templerr�stung");
registerTranslation("ITAR_ONB_07", LANG.DE, "Gururobe");
registerTranslation("ITAR_ONB_08", LANG.DE, "Hohe Guru Robe");
registerTranslation("ITAR_ONB_09", LANG.DE, "Boss Robe");
registerTranslation("ITAR_ONB_10", LANG.DE, "Boss R�stung")
registerTranslation("ITAR_PIR_01" LANG.DE, "Seemannshose");
registerTranslation("ITAR_PIR_02" LANG.DE, "Seemannskleidung");
registerTranslation("ITAR_PIR_03" LANG.DE, "Leichte Piraten-R�stung");
registerTranslation("ITAR_PIR_04" LANG.DE, "Mittlere Piraten-R�stung");
registerTranslation("ITAR_PIR_05" LANG.DE, "Schwere Piraten-R�stung");
registerTranslation("ITAR_PIR_06" LANG.DE, "R�stung des Sturmseglers");
registerTranslation("ITAR_PIR_07" LANG.DE, "Wassernovizen Robe");
registerTranslation("ITAR_PIR_08" LANG.DE, "Wassermagier Robe");
registerTranslation("ITAR_PIR_09" LANG.DE, "Boss Robe");
registerTranslation("ITAR_PIR_10" LANG.DE, "Boss R�stung");
registerTranslation("ITAR_BDT_01" LANG.DE, "Buddlerhose");
registerTranslation("ITAR_BDT_02" LANG.DE, "Leder-R�stung");
registerTranslation("ITAR_BDT_03" LANG.DE, "Leichte Banditen-R�stung");
registerTranslation("ITAR_BDT_04" LANG.DE, "Mittlere Banditen-R�stung");
registerTranslation("ITAR_BDT_05" LANG.DE, "Banditengarde-R�stung");
registerTranslation("ITAR_BDT_06" LANG.DE, "Elite-Banditen-R�stung");
registerTranslation("ITAR_BDT_07" LANG.DE, "Novizenbekleidung");
registerTranslation("ITAR_BDT_08" LANG.DE, "Nekromantenrobe");
registerTranslation("ITAR_BDT_09" LANG.DE, "Boss Robe");
registerTranslation("ITAR_BDT_10" LANG.DE, "Boss R�stung");;

/********************* ITWM *********************/

registerTranslation("ITMW_1H_01", LANG.DE, "Schwerer Ast");
registerTranslation("ITMW_1H_02", LANG.DE, "Dolch");
registerTranslation("ITMW_1H_03", LANG.DE, "Grobes Kurzschwert");
registerTranslation("ITMW_1H_04", LANG.DE, "Nagelkeule");
registerTranslation("ITMW_1H_05", LANG.DE, "Grobes Schwert");
registerTranslation("ITMW_1H_06", LANG.DE, "Schiffsaxt");
registerTranslation("ITMW_1H_07", LANG.DE, "Paladinschwert");
registerTranslation("ITMW_1H_08", LANG.DE, "Langschwert");
registerTranslation("ITMW_1H_09", LANG.DE, "Edles Langschwert");
registerTranslation("ITMW_1H_10", LANG.DE, "Edles Bastardschwert");
registerTranslation("ITMW_1H_11", LANG.DE, "Erz-Drachent�ter");
registerTranslation("ITMW_1H_12", LANG.DE, "Klaue Beliars");
registerTranslation("ITMW_2H_01", LANG.DE, "Rostiger Zweih�nder");
registerTranslation("ITMW_2H_02", LANG.DE, "Richtstab");
registerTranslation("ITMW_2H_03", LANG.DE, "Leichter Zweih�nder");
registerTranslation("ITMW_2H_04", LANG.DE, "Paladin Zweih�nder");
registerTranslation("ITMW_2H_05", LANG.DE, "Runen-Macht");
registerTranslation("ITMW_2H_06", LANG.DE, "Schwerer Zweih�nder");
registerTranslation("ITMW_2H_07", LANG.DE, "Drachenschneide");
registerTranslation("ITMW_2H_08", LANG.DE, "Heiliger Vollstrecker");
registerTranslation("ITMW_2H_09", LANG.DE, "Sturmbringer");
registerTranslation("ITMW_2H_10", LANG.DE, "Gro�er Erz Drachent�ter");
registerTranslation("ITMW_2H_11", LANG.DE, "Orkisches Kriegsschwert");

/********************* ITRW *********************/

registerTranslation("ITRW_BOW_ARROW", LANG.DE, "Pfeil");
registerTranslation("ITRW_CROSSBOW_BOLT", LANG.DE, "Bolzen");
registerTranslation("ITRW_MAGICBOW_ARROW", LANG.DE, "Magische Pfeil");
registerTranslation("ITRW_MAGICCROSSBOW_BOLT", LANG.DE, "Magische Bolzen");
registerTranslation("ITRW_BOW_01", LANG.DE, "Kurzbogen");
registerTranslation("ITRW_BOW_02", LANG.DE, "Weidenbogen");
registerTranslation("ITRW_BOW_03", LANG.DE, "Jagdbogen");
registerTranslation("ITRW_BOW_04", LANG.DE, "Ulmenbogen");
registerTranslation("ITRW_BOW_05", LANG.DE, "Kompositbogen");
registerTranslation("ITRW_BOW_06", LANG.DE, "Eschenbogen");
registerTranslation("ITRW_BOW_07", LANG.DE, "Langbogen");
registerTranslation("ITRW_BOW_08", LANG.DE, "Buchenbogen");
registerTranslation("ITRW_BOW_09", LANG.DE, "Knochenbogen");
registerTranslation("ITRW_BOW_10", LANG.DE, "Eichenbogen");
registerTranslation("ITRW_BOW_11", LANG.DE, "Magischer Bogen");
registerTranslation("ITRW_CROSSBOW_01", LANG.DE, "Alte Armbrust");
registerTranslation("ITRW_CROSSBOW_02", LANG.DE, "Leichte Armbrust");
registerTranslation("ITRW_CROSSBOW_03", LANG.DE, "Gute Armbrust");
registerTranslation("ITRW_CROSSBOW_04", LANG.DE, "Jagdarmbrust");
registerTranslation("ITRW_CROSSBOW_05", LANG.DE, "Kriegsarmbrust");
registerTranslation("ITRW_CROSSBOW_06", LANG.DE, "Verst�rkte Armbrust");
registerTranslation("ITRW_CROSSBOW_07", LANG.DE, "Elite Armbrust");
registerTranslation("ITRW_CROSSBOW_08", LANG.DE, "Schwere Armbrust");
registerTranslation("ITRW_CROSSBOW_09", LANG.DE, "Nordmarer Armbrust");
registerTranslation("ITRW_CROSSBOW_10", LANG.DE, "Drachenj�gerarmbrust");
registerTranslation("ITRW_CROSSBOW_11", LANG.DE, "Magische Armbrust");

/********************* ITRU *********************/

registerTranslation("ITRU_FIREBOLT", LANG.DE, "Feuerpfeil");
registerTranslation("ITRU_ICEBOLT", LANG.DE, "Eispfeil");
registerTranslation("ITRU_LIGHTHEAL", LANG.DE, "Leichte Wunden Heilen");
registerTranslation("ITRU_ZAP", LANG.DE, "Blitz");
registerTranslation("ITRU_MEDIUMHEAL", LANG.DE, "Mittlere Wunden Heilen");
registerTranslation("ITRU_ICELANCE", LANG.DE, "Eislanze");
registerTranslation("ITRU_HEAL_TARGET", LANG.DE, "Zielheilung");
registerTranslation("ITRU_HEAL_TARGET_DESC", LANG.DE, "Einen anderen Spieler heilen (100 Lebenspunkte)");
registerTranslation("ITRU_FIRESTORM", LANG.DE, "Kleiner Feuersturm");
registerTranslation("ITRU_FULLHEAL", LANG.DE, "Schwere Wunden Heilen");
registerTranslation("ITRU_INSTANTFIREBALL", LANG.DE, "Feuerball");
registerTranslation("ITRU_CONCUSSIONBOLT", LANG.DE, "Heiliger Pfeil");

/********************* ITSC *********************/

registerTranslation("ITSC_MASSDEATH", LANG.DE, "Todeswelle");

/********************* ITPO *********************/

registerTranslation("ITPO_HEALTH_00", LANG.DE, "Trank des Novizen");
registerTranslation("ITPO_HEALTH_01", LANG.DE, "Kleiner Heiltrank");
registerTranslation("ITPO_HEALTH_02", LANG.DE, "Mittlerer Heiltrank");
registerTranslation("ITPO_HEALTH_03", LANG.DE, "Gro�er Heiltrank");
registerTranslation("ITPO_HEALTH_04", LANG.DE, "Gro�er Heiltrank");
registerTranslation("ITPO_MANA_01", LANG.DE, "Kleiner Manatrank");
registerTranslation("ITPO_MANA_02", LANG.DE, "Mittlerer Manatrank");
registerTranslation("ITPO_MANA_03", LANG.DE, "Gro�er Manatrank");
registerTranslation("ITPO_SPEED", LANG.DE, "Trank der Geschwindigkeit");
registerTranslation("ITPO_SPEED_EFFECT", LANG.DE, "Sprint f�r 30 Sekunden");

/********************* ITMI *********************/

registerTranslation("ITMI_GOLD", LANG.DE, "Goldm�nze");
registerTranslation("ITMI_GOLD_DESC", LANG.DE, "Es ist nur eine goldm�nze...");
registerTranslation("ITMI_FOCUS_RED", LANG.DE, "Roter Fokusstein");
registerTranslation("ITMI_FOCUS_RED_DESC", LANG.DE, "Bonus: 50 Lebenspunkte - Fallenlassen der Ausr�stung, wenn man stirbt");
registerTranslation("ITMI_FOCUS_YELLOW", LANG.DE, "Gelber Fokusstein");
registerTranslation("ITMI_FOCUS_YELLOW_DESC", LANG.DE, "Bonus: 10 St�rke - Fallenlassen der Ausr�stung, wenn man stirbt");
registerTranslation("ITMI_FOCUS_GRAY", LANG.DE, "Grauer Scroll");
registerTranslation("ITMI_FOCUS_GRAY_DESC", LANG.DE, "Du verwandelst dich in einen Razor - Klicken Sie zum Abschluss auf Z - Fallenlassen der Ausr�stung, wenn man stirbt");
registerTranslation("ITMI_FOCUS_VIOLET", LANG.DE, "Violetter Scroll");
registerTranslation("ITMI_FOCUS_VIOLET_DESC", LANG.DE, "Du verwandelst dich in einen Minecrawler Warrior - Klicken Sie zum Abschluss auf Z - Fallenlassen der Ausr�stung, wenn man stirbt");
registerTranslation("ITMI_FOCUS_BLUE", LANG.DE, "Blauer Fokusstein");
registerTranslation("ITMI_FOCUS_BLUE_DESC", LANG.DE, "Bonus: 20 Mana - Fallenlassen der Ausr�stung, wenn man stirbt");
registerTranslation("ITMI_FOCUS_GREEN", LANG.DE, "Gr�ner Fokusstein");
registerTranslation("ITMI_FOCUS_GREEN_DESC", LANG.DE, "Bonus: 10 Geschick - Fallenlassen der Ausr�stung, wenn man stirbt");
