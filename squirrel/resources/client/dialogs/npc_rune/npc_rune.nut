
registerDialog(Dialog.Transcription("NPC_RUNE_INITIAL")
    .addDialog("NPC_RUNE_INITIAL_0", true, 2500)
    .addDialog("NPC_RUNE_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_RUNE_INITIAL_0")
    .setTitle("NPC_RUNE_INITIAL_0_TITLE")
    .addDialog("NPC_RUNE_INITIAL_0_0", true, 2500)
    .addDialog("NPC_RUNE_INITIAL_0_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_RUNE_INITIAL_1")
    .setTitle("NPC_RUNE_INITIAL_1_TITLE")
    .addDialog("NPC_RUNE_INITIAL_1_0", true, 2500)
    .addDialog("NPC_RUNE_INITIAL_1_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_RUNE_END")
    .setTitle("NPC_RUNE_END_TITLE")
    .addDialog("NPC_RUNE_END_0", true, 2500)
    .addDialog("NPC_RUNE_END_1", false, 2500)
);