registerTranslation("NPC_SKILL_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_SKILL_INITIAL_1", LANG.RU, "� ���� ������� ��� ����� ������ �������...");

registerTranslation("NPC_SKILL_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_SKILL_END_0", LANG.RU, "����...");
registerTranslation("NPC_SKILL_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_SKILL_TRAINING_GROUND_TITLE", LANG.RU, "� ���� �������� �������...");

registerTranslation("NPC_SKILL_1H_INITIAL_TITLE", LANG.RU, "�������� ��� � ���������� �������");
registerTranslation("NPC_SKILL_1H_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_SKILL_1H_INITIAL_1", LANG.RU, "� ���� ������� ��� ����� ������� ���������� �������...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_TITLE", LANG.RU, "���������� ������ +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_TITLE", LANG.RU, "���������� ������ +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_TITLE", LANG.RU, "���������� ������ +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_TITLE", LANG.RU, "���������� ������ +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_1H_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_SKILL_1H_END_0", LANG.RU, "����...");
registerTranslation("NPC_SKILL_1H_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_SKILL_2H_INITIAL_TITLE", LANG.RU, "�������� ��� � ��������� �������");
registerTranslation("NPC_SKILL_2H_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_SKILL_2H_INITIAL_1", LANG.RU, "� ���� ������� ��� ����� ������� ��������� �������...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_TITLE", LANG.RU, "��������� ������ +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_TITLE", LANG.RU, "��������� ������ +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_TITLE", LANG.RU, "��������� ������ +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_TITLE", LANG.RU, "��������� ������ +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_2H_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_SKILL_2H_END_0", LANG.RU, "����...");
registerTranslation("NPC_SKILL_2H_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_SKILL_BOW_INITIAL_TITLE", LANG.RU, "�������� ��� � �����");
registerTranslation("NPC_SKILL_BOW_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_SKILL_BOW_INITIAL_1", LANG.RU, "� ���� ������� ��� ����� ������� �����...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_TITLE", LANG.RU, "��� +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_TITLE", LANG.RU, "��� +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_TITLE", LANG.RU, "��� +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_TITLE", LANG.RU, "��� +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_BOW_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_SKILL_BOW_END_0", LANG.RU, "����...");
registerTranslation("NPC_SKILL_BOW_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_SKILL_CBOW_INITIAL_TITLE", LANG.RU, "�������� ��� � ���������");
registerTranslation("NPC_SKILL_CBOW_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_SKILL_CBOW_INITIAL_1", LANG.RU, "� ���� ������� ��� ����� ������� ���������...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_TITLE", LANG.RU, "�������� +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_TITLE", LANG.RU, "�������� +5, ���������: 5 LP...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_TITLE", LANG.RU, "�������� +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_TITLE", LANG.RU, "�������� +10, ���������: 10 LP...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_SKILL_CBOW_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_SKILL_CBOW_END_0", LANG.RU, "����...");
registerTranslation("NPC_SKILL_CBOW_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_STRENGTH_INITIAL_TITLE", LANG.RU, "����������� ����");
registerTranslation("NPC_STRENGTH_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_STRENGTH_INITIAL_1", LANG.RU, "� ���� ������� ����, ��� ����� �������...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_TITLE", LANG.RU, "���� +5, ���������: 5 LP...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_TITLE", LANG.RU, "���� +5, ���������: 5 LP...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_STRENGTH_TEN_POINT_TITLE", LANG.RU, "���� +10, ���������: 10 LP...");
registerTranslation("NPC_STRENGTH_TEN_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_TITLE", LANG.RU, "���� +10, ���������: 10 LP...");
registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_STRENGTH_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_STRENGTH_END_0", LANG.RU, "����...");
registerTranslation("NPC_STRENGTH_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_DEXTERITY_INITIAL_TITLE", LANG.RU, "����������� ��������");
registerTranslation("NPC_DEXTERITY_INITIAL_0", LANG.RU, "���� �� ������ ���� �������?");
registerTranslation("NPC_DEXTERITY_INITIAL_1", LANG.RU, "� ���� ������� ��� ���� ����� ������...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_TITLE", LANG.RU, "�������� +5, ���������: 5 LP...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_TITLE", LANG.RU, "�������� +5, ���������: 5 LP...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_TITLE", LANG.RU, "�������� +10, ���������: 10 LP...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_TITLE", LANG.RU, "�������� +10, ���������: 10 LP...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_DEXTERITY_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_DEXTERITY_END_0", LANG.RU, "����...");
registerTranslation("NPC_DEXTERITY_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_MANA_INITIAL_TITLE", LANG.RU, "����� ����, ��� � ���� ��������� ���� ���������� �������...");
registerTranslation("NPC_MANA_INITIAL_0", LANG.RU, "������� ����, ��� � ���� ��������� ���� ���������� �������...");
registerTranslation("NPC_MANA_INITIAL_1", LANG.RU, "�������...");

registerTranslation("NPC_MANA_FIVE_POINT_TITLE", LANG.RU, "���� +5, ���������: 5 LP...");
registerTranslation("NPC_MANA_FIVE_POINT_0", LANG.RU, "�������, �� ���-�� �����...");

registerTranslation("NPC_MANA_FIVE_POINT_CANT_TITLE", LANG.RU, "���� +5, ���������: 5 LP...");
registerTranslation("NPC_MANA_FIVE_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_MANA_TEN_POINT_TITLE", LANG.RU, "���� +10, ���������: 10 LP...");
registerTranslation("NPC_MANA_TEN_POINT_0", LANG.RU, "�������, �� ���-�� �����...");

registerTranslation("NPC_MANA_TEN_POINT_CANT_TITLE", LANG.RU, "���� +10, ���������: 10 LP...");
registerTranslation("NPC_MANA_TEN_POINT_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_MANA_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_MANA_END_0", LANG.RU, "����...");
registerTranslation("NPC_MANA_END_1", LANG.RU, "�� �������...");

registerTranslation("NPC_MAGIC_INITIAL_TITLE", LANG.RU, "����� ���� ������������ ������ ������� �����...");
registerTranslation("NPC_MAGIC_INITIAL_0", LANG.RU, "������� ���� ������������ ����� ������� �����...");
registerTranslation("NPC_MAGIC_INITIAL_1", LANG.RU, "�������...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_TITLE", LANG.RU, "���������� ���� +1, ���������: 10 LP...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_0", LANG.RU, "�������, �� ����-�� ���������...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_TITLE", LANG.RU, "���������� ���� +1, ���������: 10 LP...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_0", LANG.RU, "� �� ���� ���� ������ �������, ���� �� ������� �����...");

registerTranslation("NPC_MAGIC_END_TITLE", LANG.RU, "����...");
registerTranslation("NPC_MAGIC_END_0", LANG.RU, "����...");
registerTranslation("NPC_MAGIC_END_1", LANG.RU, "�� �������...");