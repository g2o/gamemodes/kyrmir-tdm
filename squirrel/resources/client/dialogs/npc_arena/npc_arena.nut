
registerDialog(Dialog.Transcription("NPC_ARENA_INITIAL")
    .setTitle("NPC_ARENA_INITIAL_TITLE")
    .addDialog("NPC_ARENA_INITIAL_0" , true, 2500)
    .addDialog("NPC_ARENA_INITIAL_1", false, 2500)
);

registerDialog(Dialog.Transcription("NPC_ARENA_BACK_TO_WORLD")
    .setTitle("NPC_ARENA_BACK_TO_WORLD_TITLE")
);

registerDialog(Dialog.Transcription("NPC_ARENA_END")
    .setTitle("NPC_ARENA_END_TITLE")
    .addDialog("NPC_ARENA_END_0" , true, 2500)
    .addDialog("NPC_ARENA_END_1", false, 2500)
);