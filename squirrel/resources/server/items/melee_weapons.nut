
/********************************** Weapons 1H **********************************/

addItemTemplate(
    Item.Weapon("ITMW_1H_01", Items.id("ITMW_1H_BAU_MACE"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 7)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_02", Items.id("ITMW_1H_VLK_DAGGER"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 10)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_03", Items.id("ITMW_SHORTSWORD2"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 20)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_04", Items.id("ITMW_NAGELKEULE2"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 30)
        .setRequirement(ItemRequirement.Strength, 30)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_05", Items.id("ITMW_1H_SLD_SWORD"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 40)
        .setRequirement(ItemRequirement.Strength, 40)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_06", Items.id("ITMW_SCHIFFSAXT"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 50)
        .setRequirement(ItemRequirement.Strength, 50)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_07", Items.id("ITMW_1H_PAL_SWORD"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 60)
        .setRequirement(ItemRequirement.Strength, 60)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_08", Items.id("ITMW_SCHWERT2"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 70)
        .setRequirement(ItemRequirement.Strength, 70)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_09", Items.id("ITMW_SCHWERT4"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 80)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_10", Items.id("ITMW_SCHWERT5"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 90)
        .setRequirement(ItemRequirement.Strength, 90)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_11", Items.id("ITMW_1H_SPECIAL_04"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 100)
        .setRequirement(ItemRequirement.Strength, 100)
);

addItemTemplate(
    Item.Weapon("ITMW_1H_12", Items.id("ITMW_BELIARWEAPON_1H"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 110)
        .setRequirement(ItemRequirement.Strength, 100)
);

/********************************** Weapons 2H **********************************/

addItemTemplate(
    Item.Weapon("ITMW_2H_01", Items.id("ITMW_2H_SWORD_M_01"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 11)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_02", Items.id("ITMW_RICHTSTAB"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 23)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_03", Items.id("ITMW_ZWEIHAENDER1"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 34)
        .setRequirement(ItemRequirement.Strength, 30)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_04", Items.id("ITMW_2H_PAL_SWORD"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 46)
        .setRequirement(ItemRequirement.Strength, 40)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_05", Items.id("ITMW_ZWEIHAENDER3"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 57)
        .setRequirement(ItemRequirement.Strength, 50)
);
 
addItemTemplate(
    Item.Weapon("ITMW_2H_06", Items.id("ITMW_ZWEIHAENDER4"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 69)
        .setRequirement(ItemRequirement.Strength, 60)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_07", Items.id("ITMW_DRACHENSCHNEIDE"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 80)
        .setRequirement(ItemRequirement.Strength, 70)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_08", Items.id("ITMW_2H_BLESSED_03"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 92)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_09", Items.id("ITMW_STURMBRINGER"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 103)
        .setRequirement(ItemRequirement.Strength, 90)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_10", Items.id("ITMW_2H_SPECIAL_04"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 115)
        .setRequirement(ItemRequirement.Strength, 100)
);

addItemTemplate(
    Item.Weapon("ITMW_2H_11", Items.id("ITMW_2H_ORCSWORD_02"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_2HS)   
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 125)
        .setRequirement(ItemRequirement.Strength, 100)
);

/********************************** Weapons Rapier **********************************/

addItemTemplate(
    Item.Weapon("ITMW_RAPIER_01", Items.id("ITMW_MEISTERDEGEN"), ItemType.WeaponMelee)
        .setWeaponMode(WEAPONMODE_1HS)    
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_EDGE, 100)
        .setRequirement(ItemRequirement.Strength, 100)
        .setOverlay(Mds.id("HUMANS_RAPIER_ST3.MDS"))
);

