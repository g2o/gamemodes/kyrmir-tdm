
addItemTemplate(
    Item.Standard("ITMI_GOLD", Items.id("ITMI_GOLD"), ItemType.Other)
        .setInteractionType(ItemInteraction.None)
);

// ARTIFACTS

addItemTemplate(
    Item.Standard("ITMI_FOCUS_GREEN", Items.id("ITAT_ICEGOLEMHEART"), ItemType.Other)
        .setInteractionType(ItemInteraction.None)
);

addItemTemplate(
    Item.Standard("ITMI_FOCUS_RED", Items.id("ITAT_ICEGOLEMHEART"), ItemType.Other)
        .setInteractionType(ItemInteraction.None)
);

addItemTemplate(
    Item.Standard("ITMI_FOCUS_YELLOW", Items.id("ITAT_ICEGOLEMHEART"), ItemType.Other)
        .setInteractionType(ItemInteraction.None)
);

addItemTemplate(
    Item.Standard("ITMI_FOCUS_BLUE", Items.id("ITAT_ICEGOLEMHEART"), ItemType.Other)
        .setInteractionType(ItemInteraction.None)
);

// SPECIAL ARTIFACTS - TRANSFORMATIONS
const HEALTH_TRANSFORMATION = 1500;
const STRENGTH_TRANSFORMATION = 200;

const HEALTH_TRANSFORMATION_ALEXIA = 1200;
const STRENGTH_TRANSFORMATION_ALEXIA = 340;

addItemTemplate(
    Item.Spell("ITMI_FOCUS_GRAY", Items.id("ITSC_TRFGIANTBUG"), ItemType.Scroll)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setManaUsage(0)
        .bindEffectFunc(function(target, casterId)
        {
            setPlayerInstance(casterId, "MINECRAWLER");
            setPlayerDamageType(casterId, DAMAGE_EDGE);

            setPlayerMaxHealth_Bonus(casterId, HEALTH_TRANSFORMATION - getPlayerMaxHealth(casterId));
            setPlayerStrength_Bonus(casterId, STRENGTH_TRANSFORMATION - getPlayerStrength(casterId));

            setPlayerHealth(casterId, getPlayerMaxHealth(casterId));
            
            return false;
        }
    )
);

addItemTemplate(
    Item.Spell("ITMI_FOCUS_VIOLET", Items.id("ITSC_TRFSNAPPER"), ItemType.Scroll)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setManaUsage(0)
        .bindEffectFunc(function(target, casterId)
        {
            setPlayerInstance(casterId, "SNAPPER");
            setPlayerDamageType(casterId, DAMAGE_EDGE);

            setPlayerMaxHealth_Bonus(casterId, HEALTH_TRANSFORMATION - getPlayerMaxHealth(casterId));
            setPlayerStrength_Bonus(casterId, STRENGTH_TRANSFORMATION - getPlayerStrength(casterId)); 

            setPlayerHealth(casterId, getPlayerMaxHealth(casterId));
            
            return false;
        }
    )
);