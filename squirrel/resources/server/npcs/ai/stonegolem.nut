
// STANDARD ANIMAL AI:
// stonegolem.nut
// swampshark.nut
// troll_black.nut

enum AI_STATE 
{
    NONE = -1,
    WARNING = 0, // WARNING_STATE
    FOLLOW = 1, // SKIP_WARNING_STATE
    FIGHT_CLOSE = 2,
}

local warningState = null,
    followState = null,
    fightState = null;

warningState = function(bot)
{
    local warnTime = bot.getWarnTime();

    if(warnTime == -1) 
    {
        bot.setWarnTime(getTickCount() + bot.getTemplate().getWarnTime());
        bot.setWeaponMode(WEAPONMODE_FIST);

        bot.playAni("T_WARN");
    }
    else 
    {
        if(warnTime <= getTickCount() || bot.getDistanceToEnemy() < 600)
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else if(bot.getDistanceToEnemy() < bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }
};

followState = function(bot)
{   
    local hitDistance = bot.getTemplate().getHitDistance();

    if(bot.getDistanceToEnemy() > hitDistance)
    {
        if(bot.isFollowing() == false) 
            bot.follow(hitDistance);
        else 
        {
            //
        }
    }
    else 
    {
        bot.setFight(AI_STATE.FIGHT_CLOSE);
            fightState(bot);
    }
};

fightState = function(bot)
{
    bot.setWeaponMode(WEAPONMODE_FIST);

    local enemyDistance = bot.getDistanceToEnemy();

    local template = bot.getTemplate();
    local hitDistance = template.getHitDistance();

    if(enemyDistance <= hitDistance)
    {    
        if(bot.isInAttackMode() == false)
            bot.attackFist(template.getAttackSpeed());
        else 
        {
            if(enemyDistance <= (hitDistance * 0.75))
                bot.parade(PARADE.JUMPB);
        }
    } 
    else 
    {
        bot.setFight(AI_STATE.FOLLOW);
            followState(bot);
    }
};

STONEGOLEM_AI <- function(bot)
{
    local state = bot.getFight();
    if(state == -1) bot.setFight(AI_STATE.WARNING);

    switch(state)
    {
        case AI_STATE.WARNING: 
            warningState(bot); 
        break;
        case AI_STATE.FOLLOW: 
            followState(bot); 
        break;
        case AI_STATE.FIGHT_CLOSE: 
            fightState(bot); 
        break;
    }
};