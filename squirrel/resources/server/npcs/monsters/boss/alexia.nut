
local ALEXIA = Bot.Template("ALEXIA_MIDGAR", "KYRMIR_ALEXIA"); 

ALEXIA.setLevel(0);
ALEXIA.setMagicLevel(0);

ALEXIA.setHealth(12000);
ALEXIA.setRespawnTime(1800)

ALEXIA.setMeleeWeapon(Items.id("ITMW_MEISTERDEGEN"));

ALEXIA.setSkillWeapon(WEAPON_1H, 100);
ALEXIA.setSkillWeapon(WEAPON_2H, 100);

ALEXIA.setDamage(DAMAGE_EDGE, 220);

ALEXIA.setProtection(DAMAGE_EDGE, 100);
ALEXIA.setProtection(DAMAGE_BLUNT, 100);
ALEXIA.setProtection(DAMAGE_FIRE, 100);
ALEXIA.setProtection(DAMAGE_MAGIC, 130);
ALEXIA.setProtection(DAMAGE_POINT, 130);

// Fight system
ALEXIA.setWarnTime(6);
ALEXIA.setHitDistance(260);
ALEXIA.setChaseDistance(2200);
ALEXIA.setDetectionDistance(1200);
ALEXIA.setAttackSpeed(340);

ALEXIA.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    bot.playAni("S_LGUARD");
});

ALEXIA.setInitiativeFunction(RAPIER_AI);

registerMonsterTemplate("KYRMIR_ALEXIA", ALEXIA);

// Drop
registerMonsterReward("KYRMIR_ALEXIA", Reward.Content(5000)
    .addDrop(Reward.Drop("ITMW_RAPIER_01", 1, 2))
);

spawnBot(ALEXIA, 7305.16, 5683.91, -29634.5, "COLONY.ZEN");
spawnBot(ALEXIA, -3847, 686, -1876, "ADDONWORLD.ZEN");