
local MAYAZOMBIE02 = Bot.Template("ZOMBIE_BOSS_RED", "KYRMIR_BOSS_RED"); 

MAYAZOMBIE02.setLevel(0);
MAYAZOMBIE02.setMagicLevel(6);

MAYAZOMBIE02.setHealth(30000);
MAYAZOMBIE02.setDamage(DAMAGE_EDGE, 180);
MAYAZOMBIE02.setRespawnTime(1800)

MAYAZOMBIE02.setProtection(DAMAGE_EDGE, 0);
MAYAZOMBIE02.setProtection(DAMAGE_BLUNT, 500);
MAYAZOMBIE02.setProtection(DAMAGE_FIRE, 0);
MAYAZOMBIE02.setProtection(DAMAGE_MAGIC, 0);
MAYAZOMBIE02.setProtection(DAMAGE_FIRE, 0);

// Fight system
MAYAZOMBIE02.setWarnTime(6);
MAYAZOMBIE02.setHitDistance(200);
MAYAZOMBIE02.setChaseDistance(3600);
MAYAZOMBIE02.setDetectionDistance(1200);
MAYAZOMBIE02.setAttackSpeed(2100);

MAYAZOMBIE02.setInitiativeFunction(STANDARD_ANIMAL_AI);

MAYAZOMBIE02.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_BOSS_RED", MAYAZOMBIE02);

// Drop
registerMonsterReward("KYRMIR_BOSS_RED", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITRW_CROSSBOW_11", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(MAYAZOMBIE02, -13003.1, 2030.31, -34660.8, "COLONY.ZEN"));
registerBoss(spawnBot(MAYAZOMBIE02, 27242, -2105, -19657, "ADDONWORLD.ZEN"));