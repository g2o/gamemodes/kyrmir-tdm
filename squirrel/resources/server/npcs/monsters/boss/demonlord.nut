
local DEMONLORD = Bot.Template("DEMON_YELLOW", "KYRMIR_BOSS_YELLOW"); 

DEMONLORD.setLevel(0);
DEMONLORD.setMagicLevel(6);

DEMONLORD.setHealth(30000);
DEMONLORD.setDamage(DAMAGE_EDGE, 180);
DEMONLORD.setRespawnTime(1800)

DEMONLORD.setProtection(DAMAGE_EDGE, 0);
DEMONLORD.setProtection(DAMAGE_BLUNT, 500);
DEMONLORD.setProtection(DAMAGE_FIRE, 0);
DEMONLORD.setProtection(DAMAGE_MAGIC, 0);
DEMONLORD.setProtection(DAMAGE_FIRE, 0);

// Fight system
DEMONLORD.setWarnTime(6);
DEMONLORD.setHitDistance(240);
DEMONLORD.setChaseDistance(3600);
DEMONLORD.setDetectionDistance(1200);
DEMONLORD.setAttackSpeed(2100);

DEMONLORD.setInitiativeFunction(STANDARD_ANIMAL_AI);

DEMONLORD.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_BOSS_YELLOW", DEMONLORD);

// Drop
registerMonsterReward("KYRMIR_BOSS_YELLOW", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITMW_1H_12", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(DEMONLORD, -33466.9, 2650.78, -21308.6, "COLONY.ZEN"));
registerBoss(spawnBot(DEMONLORD, 4686, -964, -666, "ADDONWORLD.ZEN"));