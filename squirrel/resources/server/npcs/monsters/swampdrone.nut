local SWAMPDRONE = Bot.Template("SWAMPDRONE", "KYRMIR_SWAMPDRONE"); 

SWAMPDRONE.setLevel(0);
SWAMPDRONE.setMagicLevel(0);

SWAMPDRONE.setHealth(50);
SWAMPDRONE.setRespawnTime(60)

SWAMPDRONE.setDamage(DAMAGE_EDGE, 25);

SWAMPDRONE.setProtection(DAMAGE_EDGE, 0);
SWAMPDRONE.setProtection(DAMAGE_BLUNT, 500);
SWAMPDRONE.setProtection(DAMAGE_FIRE, 0);
SWAMPDRONE.setProtection(DAMAGE_MAGIC, 10);
SWAMPDRONE.setProtection(DAMAGE_POINT, 20);

// Fight system
SWAMPDRONE.setWarnTime(6);
SWAMPDRONE.setHitDistance(180);
SWAMPDRONE.setChaseDistance(1800);
SWAMPDRONE.setDetectionDistance(1200);
SWAMPDRONE.setAttackSpeed(1800);

SWAMPDRONE.setInitiativeFunction(STANDARD_ANIMAL_AI);

SWAMPDRONE.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 2))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("T_STAND_2_EAT"); break;	
            case 2: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 7);

registerMonsterTemplate("KYRMIR_SWAMPDRONE", SWAMPDRONE);

// Drop
registerMonsterReward("KYRMIR_SWAMPDRONE", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
