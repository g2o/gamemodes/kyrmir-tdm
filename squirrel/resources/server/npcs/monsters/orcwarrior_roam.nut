
local ORCWARRIOR_ROAM = Bot.Template("ORCWARRIOR_ROAM", "KYRMIR_ORCWARRIOR_ROAM"); 

ORCWARRIOR_ROAM.setLevel(0);
ORCWARRIOR_ROAM.setMagicLevel(0);

ORCWARRIOR_ROAM.setHealth(1000);
ORCWARRIOR_ROAM.setRespawnTime(300);

ORCWARRIOR_ROAM.setMeleeWeapon(Items.id("ITMW_2H_ORCAXE_01"));
ORCWARRIOR_ROAM.setSkillWeapon(WEAPON_2H, 100);

ORCWARRIOR_ROAM.setDamage(DAMAGE_EDGE, 130);

ORCWARRIOR_ROAM.setProtection(DAMAGE_EDGE, 0);
ORCWARRIOR_ROAM.setProtection(DAMAGE_BLUNT, 500);
ORCWARRIOR_ROAM.setProtection(DAMAGE_FIRE, 0);
ORCWARRIOR_ROAM.setProtection(DAMAGE_MAGIC, 0);
ORCWARRIOR_ROAM.setProtection(DAMAGE_POINT, 0);

// Fight system
ORCWARRIOR_ROAM.setWarnTime(6);
ORCWARRIOR_ROAM.setHitDistance(240);
ORCWARRIOR_ROAM.setChaseDistance(3600);
ORCWARRIOR_ROAM.setDetectionDistance(1300);
ORCWARRIOR_ROAM.setAttackSpeed(360);

ORCWARRIOR_ROAM.setInitiativeFunction(TWO_HAND_AI);

ORCWARRIOR_ROAM.setRoutineFunction(function(bot)
{
{
    if(bot.getLastAni() == "S_WALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 1))
        {
            case 0: bot.playAni("T_STAND_2_EAT"); break;	
            case 1: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_WALKL");			
    }
}
});

registerMonsterTemplate("KYRMIR_ORCWARRIOR_ROAM", ORCWARRIOR_ROAM);

// Drop
registerMonsterReward("KYRMIR_ORCWARRIOR_ROAM", Reward.Content(400)
    .addDrop(Reward.Drop("ITMI_GOLD", 200, 200))
);

// Spawnlist
spawnBot(ORCWARRIOR_ROAM, 1348, -995, -6429, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, 556, -957, -7864, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -1507, -977, -6742, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -2398, -954, -5262, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, 602, -647, -10054, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -1809, -725, -9382, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -3287, -614, -9692, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, 2855, -714, -8656, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -10791, -5197, -18835, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -9726, -5165, -22929, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -7260, -4819, -23935, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -14800, -3451, -14472, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -14825, -3424, -14582, "ADDONWORLD.ZEN");

spawnBot(ORCWARRIOR_ROAM, -24251.4, 4776.75, -25722, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -21832.1, 4816.53, -25419.4, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -18909.6, 4859.75, 30399.2, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -19044.9, 4858.49, 30353.6, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -19401.4, 4134.81, 30598.9, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, 29143.6, -2476.27, 6373.04, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -12007.4, 2331.78, -36229.1, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -12189.9, 2262.97, -35750, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -20748, 2502.83, -19796, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -21614.3, 2506.25, -19850.6, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -22602, 2572.12, -20025, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -22898, 2656.84, -20900, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -23747, 2744.96, -20762, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -25290, 2875.14, -18542, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -24992.9, 2820.31, -19056.9, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -23713, 2896.46, -22404, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -24209, 3107.05, -23834, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -25063, 3205.5, -24120, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -25557, 3263.76, -24706, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -26531, 3301.01, -23874, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -27679, 3326.77, -24794, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -27812.4, 3230.6, -25803.7, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -26404.7, 2692.4, -26351, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -27135.8, 2702.97, -27409.9, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -26539.5, 2702.73, -28320, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -28136.9, 2712.58, -27834.2, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -31328.8, 2588.59, -27437, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -31757.7, 2588.67, -27890.1, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -32343, 2774.64, -25106, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -33004, 2761.53, -25465, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -32777, 2594.44, -21665, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -33017, 2603.49, -21829, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -33295, 2606.82, -21835, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -33590, 2653.05, -21969.8, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -33852.9, 2653.2, -21873.4, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, 17324.6, 5926.36, -30496.8, "COLONY.ZEN");
spawnBot(ORCWARRIOR_ROAM, -36132.8, 3268.03, -8432.65, "COLONY.ZEN");

