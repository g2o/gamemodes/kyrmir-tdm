
local NPC_CROSSBOW = createNpc("NPC_CROSSBOW", "PC_HERO", "ADDONWORLD.ZEN");

NPC_CROSSBOW.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Fatbald", 110);
NPC_CROSSBOW.equipArmor(Items.id("ITAR_LEATHER_L"));
NPC_CROSSBOW.equipRangedWeapon(Items.id("ITRW_CROSSBOW_L_01"));

NPC_CROSSBOW.playAni("S_HGUARD");

NPC_CROSSBOW.setPosition(30928, -4462, 9234);
NPC_CROSSBOW.setAngle(350);
NPC_CROSSBOW.setCollision(false);

NPC_CROSSBOW.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_CROSSBOW_TRADE = Dialog.TradeOption("NPC_CROSSBOW_INITIAL", "ITMI_GOLD")
    .addItem("ITRW_CROSSBOW_BOLT", 1)
    .addItem("ITRW_CROSSBOW_01", 200)
    .addItem("ITRW_CROSSBOW_02", 400)
    .addItem("ITRW_CROSSBOW_03", 600)
    .addItem("ITRW_CROSSBOW_04", 800)
    .addItem("ITRW_CROSSBOW_05", 1000)
    .addItem("ITRW_CROSSBOW_06", 1200)
    .addItem("ITRW_CROSSBOW_07", 1400)
    .addItem("ITRW_CROSSBOW_08", 1600)
    .addItem("ITRW_CROSSBOW_09", 1800)
    .addItem("ITRW_CROSSBOW_10", 2000);

registerDialogNpc(NPC_CROSSBOW.getId(), function(playerId) {
    return NPC_CROSSBOW_TRADE;
});